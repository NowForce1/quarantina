package com.snap.apis.isolationguard.forms

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.snap.apis.isolationguard.R
import com.snap.apis.isolationguard.utils.extensions.inflateChild
import com.snap.apis.isolationguard.utils.lifecycle.ViewModelFactory
import kotlinx.android.synthetic.main.form_layout.*

class FormsFragment : Fragment() {
    private val viewModel: FormsViewModel by lazy { ViewModelFactory(this).create(FormsViewModel::class.java) }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflateChild(R.layout.form_layout)
        val adapter = FormAdapter()
        formRecycler.adapter = adapter

        viewModel.liveData.observe(this, Observer { list ->
            if (list != null) adapter.submitList(list)
        })

        return view
    }
}

class FormAdapter : ListAdapter<FieldSpec<*>, FieldViewHolder<*>>(object : DiffUtil.ItemCallback<FieldSpec<*>>() {
    override fun areItemsTheSame(oldItem: FieldSpec<*>, newItem: FieldSpec<*>): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: FieldSpec<*>, newItem: FieldSpec<*>): Boolean = oldItem == newItem
}) {
    private val factory = FieldsFactory()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FieldViewHolder<*> {
        return factory.createForField(parent, FieldType.valueOf(viewType))
    }

    override fun getItemViewType(position: Int): Int = getItem(position).type.ordinal

    override fun onBindViewHolder(holder: FieldViewHolder<*>, position: Int) {
        val item = getItem(position)
        factory.bindItem(holder, item)
    }
}

class FormsViewModel private constructor(val context: Context) : ViewModel() {
    val liveData = MutableLiveData<List<FieldSpec<*>>>()
}
