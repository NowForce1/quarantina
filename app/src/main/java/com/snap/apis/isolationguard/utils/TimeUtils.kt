@file:Suppress("unused")

package com.snap.apis.isolationguard.utils

import android.os.Parcel
import android.os.Parcelable
import android.text.format.DateUtils
import com.snap.apis.isolationguard.utils.TimeUtils.now
import java.util.*

object TimeUtils {
    val now: Long get() = System.currentTimeMillis()
    val nowInSec: Long = now / DateUtils.SECOND_IN_MILLIS
    fun secSince(t: Long) = Dt(now - t, TimeUnit.MS).sec
}

enum class TimeUnit(val inMSec: Long) {
    MS(1L),
    SEC(DateUtils.SECOND_IN_MILLIS),
    MIN(DateUtils.MINUTE_IN_MILLIS),
    H(DateUtils.HOUR_IN_MILLIS),
    DAY(DateUtils.DAY_IN_MILLIS),
    WEEK(DateUtils.WEEK_IN_MILLIS),
    YEAR(DateUtils.YEAR_IN_MILLIS)
}

@Suppress("MemberVisibilityCanBePrivate")
data class Dt(val t: Long) : Parcelable {
    constructor(t: Number, unit: TimeUnit) : this(when (t) {
        is Int, is Long -> (t.toLong() * unit.inMSec)
        is Float, is Double -> (t.toDouble() * unit.inMSec).toLong()
        else -> (t.toLong() * unit.inMSec)
    })

    constructor(parcel: Parcel) : this(parcel.readLong())

    val ago get() = Dt(now - t)
    val fromNow get() = Dt(now + t)
    val ms get() = t / TimeUnit.MS.inMSec
    val sec get() = t / TimeUnit.SEC.inMSec
    val min get() = t / TimeUnit.MIN.inMSec
    val h get() = t / TimeUnit.H.inMSec
    val days get() = t / TimeUnit.DAY.inMSec

    data class HMS(val hours: Int, val minutes: Int, val seconds: Int) {
        override fun toString(): String = "$hours:$minutes:$seconds"
    }

    val hms get() = HMS((h).toInt(), (min % 60).toInt(), (sec % 60).toInt())
    val asDate get() = Date(t)

    operator fun plus(other: Dt) = Dt(t + other.t)
    operator fun minus(other: Dt) = Dt(t - other.t)
    operator fun times(by: Int): Dt = Dt(t * by)
    operator fun div(by: Int): Dt = Dt(t / by)

    operator fun compareTo(other: Dt): Int = t.compareTo(other.t)
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(t)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Dt> {
        override fun createFromParcel(parcel: Parcel): Dt {
            return Dt(parcel)
        }

        override fun newArray(size: Int): Array<Dt?> {
            return arrayOfNulls(size)
        }
    }

    infix fun plusMinus(v: Dt) = MathUtils.randomAbout(t, v.ms).ms
}

val Number.ms get() = Dt(this, TimeUnit.MS)
val Number.sec get() = Dt(this, TimeUnit.SEC)
val Number.min get() = Dt(this, TimeUnit.MIN)
val Number.hours get() = Dt(this, TimeUnit.H)
val Number.days get() = Dt(this, TimeUnit.DAY)
val Number.weeks get() = Dt(this, TimeUnit.WEEK)
val Number.years get() = Dt(this, TimeUnit.YEAR)
infix fun Number.inMsIsOlderThan(duration: Dt) = (ms + duration).ms < now

suspend fun delay(dt: Dt) = kotlinx.coroutines.delay(dt.ms)