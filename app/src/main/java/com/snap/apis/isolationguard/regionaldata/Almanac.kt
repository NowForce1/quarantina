package com.snap.apis.isolationguard.regionaldata

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.snap.apis.isolationguard.api.HttpClient
import com.snap.apis.isolationguard.location.AlmanacEntry
import com.snap.apis.isolationguard.location.LocationStorageModel
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.*
import com.snap.apis.isolationguard.utils.extensions.mapNotNull
import com.snap.apis.isolationguard.utils.extensions.orIfNullOrEmpty
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import org.json.JSONObject


class Almanac private constructor(context: Context) {
    @SuppressLint("SimpleDateFormat")
    private fun checkAlmanacDataFromServer(data: AlmanacEntry): Boolean =
        data.lat in -90.0..90.0 && data.lon in -180.0..180.0 && data.fromTs > -1L && data.toTs > -1L

    private fun parseAlmanacItem(item: JSONObject): AlmanacEntry? = AlmanacEntry(
        id = item.optLong("ID"),
        name = item.optString("Name"),
        address = item.optString("Address"),
        notes = item.optString("Notes"),
        fromTs = item.optLong("FromDateTime") * DateUtils.SECOND_IN_MILLIS,
        toTs = item.optLong("ToDateTime") * DateUtils.SECOND_IN_MILLIS,
        locationType = item.optString("LocationType"),
        lon = item.optDouble("Long", 1000.0),
        lat = item.optDouble("Lat", 1000.0)
    ).takeIf(this::checkAlmanacDataFromServer)

    fun downloadAlmanac(context: Context): Boolean {
        val stopWatch = Stopwatch()
        statusLiveData.postValue(DownloadStatus.Working())
        val retentionPeriod = PublicConfig.get()?.retentionPeriod ?: 14.days
        val url = PublicConfig.get()?.locationRepositoryUrl.orIfNullOrEmpty(CommonConsts.Uris.ALMANAC_DEFAULT_URI).replace("http:", "https:") + "?TimeStamp=${TimeUtils.nowInSec}"
        val response = HttpClient().get(url, HttpClient.Headers("Content-Type" to "application/json"))
        when (response) {
            is HttpClient.SuccessHttpResponse -> {
                val data: List<AlmanacEntry> = JSONArray(response.result).mapNotNull(this::parseAlmanacItem)
                runBlocking {
                    LocationStorageModel().getDatabase(context).almanacDao().update(retentionPeriod.ago.ms, data)
                }
                lastFetched = TimeUtils.now.also { Prefs.write(context, "AlmanacSavedAt", it) }
                statusLiveData.postValue(DownloadStatus.Success(TimeUtils.now))
            }
            is HttpClient.FailedHttpResponse -> {
                WorkerServiceCallBuilder.scheduleAlmanacRefresh(context, 20.min plusMinus 10.min).dispatch()
                statusLiveData.postValue(DownloadStatus.Failed(response.message))
            }
            else -> {
                statusLiveData.postValue(DownloadStatus.Failed("N/A"))
            }
        }

        val message = when (response) {
            is HttpClient.SuccessHttpResponse -> "Success"
            is HttpClient.FailedHttpResponse -> "Failure (${response.message})"
            else -> "N/A"
        }

        Log.v(LOG_TAG, "Almanac download: $message in $stopWatch sec")
        return response is HttpClient.SuccessHttpResponse
    }

    private var lastFetched: Long = Prefs.read(context, "AlmanacSavedAt", -1L)

    fun isOlderThan(age: Dt): Boolean = lastFetched inMsIsOlderThan age

    sealed class DownloadStatus {
        class Idle : DownloadStatus()
        class Working : DownloadStatus()
        class Success(val updatedTs: Long) : DownloadStatus()
        class Failed(reason: String) : DownloadStatus()
    }

    companion object {
        private const val LOG_TAG = "AlmanacHandler"
        private var instance: Almanac? = null
        val statusLiveData = MutableLiveData<DownloadStatus>().apply { postValue(DownloadStatus.Idle()) }
        fun get(context: Context) = instance ?: Almanac(context).also { instance = it }
    }
}
