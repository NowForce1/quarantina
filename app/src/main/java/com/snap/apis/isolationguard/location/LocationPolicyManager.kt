package com.snap.apis.isolationguard.location

import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import com.snap.apis.isolationguard.utils.Dt
import com.snap.apis.isolationguard.utils.ms
import java.util.concurrent.ConcurrentHashMap

abstract class LocationPolicyManager protected constructor(aContext: Context) {
    suspend fun onLocation(intent: Intent) {
        val location = LocationDriver.decodeIntent(intent) ?: return
        val dtFromThatSource = (location.time - (lastAcceptedReadouts[location.provider]?.time ?: 0)).ms
        if (isAccepted(location, dtFromThatSource)) {
            onLocation(location)
            lastAcceptedReadouts[location.provider] = location
        }
    }

    protected abstract suspend fun onLocation(location: Location)
    protected open fun isAccepted(location: Location, dtFromThatSource: Dt) = true
    protected val context: Context = aContext.applicationContext

    protected fun locationFromEntry(entry: LocationEntry) = Location(entry.provider).apply {
        accuracy = entry.accuracy
        time = entry.ts
        latitude = entry.lat
        longitude = entry.lon
    }

    protected fun credibilityTest(location: Location): Boolean {
        val diversionFromGps: Float = lastAcceptedReadouts[LocationManager.GPS_PROVIDER]?.distanceTo(location) ?: 0f
        val newerThanGpsBy = location.time - (lastAcceptedReadouts[LocationManager.GPS_PROVIDER]?.time ?: 0)
        return ((newerThanGpsBy.ms.sec * 10) > diversionFromGps)
    }

    abstract fun setup(context: Context)

    private val lastAcceptedReadouts = ConcurrentHashMap<String, Location>()
}