package com.snap.apis.isolationguard.utils.extensions

/**
 * Description:
 * Created by shmuel on 2019-06-17.
 */
inline fun Boolean.orElse(f: () -> Any) = this.also { if (!it) f.invoke() }

fun String?.orIfNullOrEmpty(alternative: String): String = if (this?.isNotEmpty() == true) this else alternative

val Boolean.asSuccess get() = if (this) "Success" else "Failure"

val String.sumHash get() = fold(0, { acc, c -> acc * 256 + c.toInt() })

