package com.snap.apis.isolationguard.utils

import kotlin.math.PI
import kotlin.random.Random

object MathUtils {
    private val rnd by lazy { Random(System.nanoTime()) }

    /**
     * Uniform random e±random(diversion)
     */
    fun randomAbout(e: Long, diversion: Long) = e + rnd.nextLong(-diversion, diversion)
}

/**
 * Degrees to radians extension
 */
val Number.deg get() = toDouble() * PI / 180.0
