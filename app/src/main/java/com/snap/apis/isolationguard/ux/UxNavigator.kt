package com.snap.apis.isolationguard.ux

import android.app.Activity
import com.snap.apis.isolationguard.GreenActivity
import com.snap.apis.isolationguard.RedActivity
import com.snap.apis.isolationguard.YellowActivity
import com.snap.apis.isolationguard.state.SystemState

class UxNavigator {
    fun getActivity(state: SystemState.State): Class<out Activity> = when (state) {
        SystemState.State.Unknown -> GreenActivity::class.java
        SystemState.State.GreenSilent -> GreenActivity::class.java
        SystemState.State.GreenActive -> GreenActivity::class.java
        SystemState.State.Yellow -> YellowActivity::class.java
        SystemState.State.Red -> RedActivity::class.java
    }
}