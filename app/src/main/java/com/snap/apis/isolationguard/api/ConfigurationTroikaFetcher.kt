package com.snap.apis.isolationguard.api

import com.google.gson.Gson
import com.snap.apis.isolationguard.utils.extensions.toNfUri

object ConfigurationTroikaFetcher {
    fun fetch(cached: Boolean = true, general: Boolean = false, ctd: Boolean = false, orgConfigs: Boolean = false): ConfigurationTroika? {
        return LoggedInConfig.troika?.takeIf { cached } ?: let {
            val args = mutableListOf<Pair<String, String>>()
            if (general) args += "General" to "true"
            if (ctd) args += "CodeTableDataItems" to "5"
            if (orgConfigs) args += "OrganizationConfigurations" to "true"

            val config =
                (LoggedInConfig.transceiver.get("User/Configurations2/json/#org/#org".toNfUri(args)) as? HttpClient.SuccessHttpResponse)?.result
                    ?: return@let null
            Gson().fromJson(config, ConfigurationTroika::class.java)
        }
    }
}