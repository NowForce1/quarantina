package com.snap.apis.isolationguard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.snap.apis.isolationguard.advertiser.AdvertiserId
import com.snap.apis.isolationguard.api.Authenticator
import com.snap.apis.isolationguard.api.LoggedInConfig
import com.snap.apis.isolationguard.utils.extensions.str
import com.snap.apis.isolationguard.utils.launchUI
import com.snap.apis.isolationguard.utils.safeResume
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.Deferred
import java.util.*
import kotlin.coroutines.suspendCoroutine

class LoginActivity : AppCompatActivity() {
    private val viewModel: LoginViewModel by lazy { ViewModelProvider.NewInstanceFactory().create(LoginViewModel::class.java).also { it.context = this } }
    private val busyLive = MutableLiveData<Boolean>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        Log.v("LoginActivity", "Start")

        loginButton.setOnClickListener {
            busyLive.postValue(true)
            launchUI {
                val activity = this@LoginActivity
                val clearToGo = viewModel.loginSequence(userNameText.str, passwordText.str, serverNameText.str) { handleAuthResult(it) }
                busyLive.postValue(false)
                if (clearToGo) {
                    startActivity(Intent(activity, YellowActivity::class.java))
                    finish()
                }
            }
        }

        busyLive.observe(this, androidx.lifecycle.Observer { busy ->
            loginBusy.visibility = if (busy == true) View.VISIBLE else View.GONE
            loginButton.isEnabled = busy == false
            loginButton.alpha = if (busy == true) 0.3f else 1.0f
        })
    }

    private suspend fun handleAuthResult(resolution: LoginViewModel.AuthResult): Boolean {
        return when (resolution) {
            LoginViewModel.AuthResult.GeneralFailure -> {
                showFailureDialogue(R.string.generalFailureToLogin); false
            }
            LoginViewModel.AuthResult.WrongUsernameAndPassword -> {
                showFailureDialogue(R.string.wrongCredentialsError); false
            }
            LoginViewModel.AuthResult.AlreadyLoggedIn -> showAlreadyLoggedInDialogue()
            LoginViewModel.AuthResult.Success -> true
        }
    }

    private suspend fun showFailureDialogue(messageStrId: Int) {
        suspendCoroutine<Boolean> { c ->
            AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(messageStrId)
                .setOnDismissListener { c.safeResume(false) }.show()
        }
    }

    private suspend fun showAlreadyLoggedInDialogue(): Boolean = suspendCoroutine { c ->
        AlertDialog.Builder(this).setTitle(R.string.warning).setMessage(R.string.alreadyLogInMessage)
            .setPositiveButton(android.R.string.ok) { _, _ -> c.safeResume(true) }
            .setNegativeButton(android.R.string.cancel) { _, _ -> c.safeResume(false) }
            .setOnDismissListener { c.safeResume(false) }.show()
    }
}

class LoginViewModel : ViewModel() {
    enum class AuthResult {
        Success,
        AlreadyLoggedIn,
        WrongUsernameAndPassword,
        GeneralFailure
    }

    lateinit var context: Context
    private val advertiserIdDeferred: Deferred<String?> = AdvertiserId.determineAdvertisingInfoAsync(context)
    private fun resolve(authResult: Authenticator.AuthResult): AuthResult = if (authResult is Authenticator.AuthResult.AuthFailure) {
        if (authResult.authError?.errorType == 1) AuthResult.WrongUsernameAndPassword else AuthResult.GeneralFailure
    } else if (authResult is Authenticator.AuthResult.AuthSuccess) {
        if (authResult.isLoggedIn) AuthResult.AlreadyLoggedIn else AuthResult.Success
    } else {
        AuthResult.GeneralFailure
    }

    suspend fun loginSequence(user: String, password: String, serverUrl: String, onHandleEvents: suspend (AuthResult) -> Boolean): Boolean {
        if (arrayOf(user, password, serverUrl).any { it.isBlank() }) return false

        val authResult = authenticate(user, password, serverUrl)

        val continueToLogin = onHandleEvents(resolve(authResult))
        if (!continueToLogin) {
            return false
        }

        val loginResult = (authResult as? Authenticator.AuthResult.AuthSuccess)?.let { login(it) } as? Authenticator.LoginResult.LoginSuccess
        return if (loginResult?.isValid == true) {
            LoggedInConfig.store(loginResult.userDetails, loginResult.transceiver, loginResult.serverPrefix)
            true
        } else false
    }

    private suspend fun authenticate(userName: String, password: String, server: String): Authenticator.AuthResult = Authenticator.authenticate(userName, password, server)

    private suspend fun login(authResult: Authenticator.AuthResult.AuthSuccess): Authenticator.LoginResult {
        val versionInfo = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        return Authenticator.login(authResult, Authenticator.AppData(versionInfo, getRandomUDID(), advertiserIdDeferred.await()))
    }

    private fun getRandomUDID(): String {
        return UUID.randomUUID().toString()
    }
}
