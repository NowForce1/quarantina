package com.snap.apis.isolationguard

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat

class Notifications(aContext: Context) {
    private val context = aContext.applicationContext

    fun foregroundNotification(): Notification {
        setChannelIfRequired()
        fun getPendingIntentForEternalNotification(context: Context): PendingIntent {
            val intent = Intent(context.applicationContext, SplashActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            return PendingIntent.getActivity(context, 0xF9, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val pendingIntent = getPendingIntentForEternalNotification(context)
        val builder = NotificationCompat.Builder(context, channelIds.permanentChannelId)
            .setSmallIcon(R.drawable.ic_plaster_white_24dp)
            .setAutoCancel(false)
            .setOngoing(true)
            .setContentTitle(context.getString(R.string.app_name))
            .setContentText(context.getString(R.string.tapToOpenTheApp))
            .setLocalOnly(true)
            .setContentIntent(pendingIntent)

        return builder.build()
    }

    private fun setChannelIfRequired() {
        if (isFirstInit) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channelsList = mutableListOf<NotificationChannel>()
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val permanentChannel = NotificationChannel(channelIds.permanentChannelId, context.getString(R.string.app_name), NotificationManager.IMPORTANCE_MIN)
                permanentChannel.setSound(null, null)
                permanentChannel.setShowBadge(false)
                permanentChannel.description = "Foreground service notification"
                permanentChannel.importance = NotificationManager.IMPORTANCE_MIN
                channelsList.add(permanentChannel)

                val encountersChannel = NotificationChannel(channelIds.newEncountersChannelId, context.getString(R.string.newEncountersString), NotificationManager.IMPORTANCE_LOW)
                encountersChannel.setSound(null, null)
                encountersChannel.enableLights(true)
                encountersChannel.lightColor = 0x0080FF
                encountersChannel.description = context.getString(R.string.newEncountersString)
                channelsList.add(encountersChannel)
                notificationManager.createNotificationChannels(channelsList)
            }
            isFirstInit = false
        }
    }

    fun notifyNewData() {
        setChannelIfRequired()
        fun getPendingIntentForEternalNotification(context: Context): PendingIntent {
            val intent = Intent(context.applicationContext, GreenActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            return PendingIntent.getActivity(context, 0xF9, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val pendingIntent = getPendingIntentForEternalNotification(context)
        val title = context.getString(R.string.app_name)
        val builder = NotificationCompat.Builder(context, channelIds.newEncountersChannelId)
            .setSmallIcon(R.drawable.ic_security_black_24dp)
            .setAutoCancel(true)
            .setOngoing(false)
            .setContentTitle(title)
            .setContentText(context.getString(R.string.newSuspectedIncidents))
            .setLocalOnly(true)
            .setContentIntent(pendingIntent)

        val notification = builder.build()
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(1, notification)
    }

    companion object {
        var isFirstInit = true

        data class ChannelIds(var permanentChannelId: String = "Main", var newEncountersChannelId: String = "Encounters")

        val channelIds = ChannelIds()
    }
}
