package com.snap.apis.isolationguard.utils

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent

class ActivityLogger(private val tagName: String, private val lifeCycle: Lifecycle) {
    fun monitor() {
        Log.v(tagName, "Monitoring (${lifeCycle.currentState})")
        lifeCycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
            fun onEvent(owner: LifecycleOwner, event: Lifecycle.Event) {
                Log.v(tagName, "Event: $event")
            }
        })
    }

    fun log(string: String) {
        Log.v(tagName, string)
    }
}