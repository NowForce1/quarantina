package com.snap.apis.isolationguard.utils.extensions

import android.net.Uri
import com.snap.apis.isolationguard.api.LoggedInConfig

/**
 * Description:
 * Created by shmuel on 2019-06-17.
 */

object NfUri {
    fun substitute(from: String): String = LoggedInConfig.userDetails.let { u ->
        from.replace("#org", u.orgId.toString()).replace("#uid", u.userId.toString())
    }
}

fun Uri.Builder.append(string: String): Uri.Builder {
    val pathAndQuery = string.split("?")
    pathAndQuery.getOrNull(0)?.let { appendEncodedPath(it) }
    pathAndQuery.getOrNull(1)?.let { it.split("&").forEach { seg -> appendQueryParameter(seg.substringBefore('='), seg.substringAfter('=')) } }
    return this
}

fun String.toNfUri(): String = Uri.parse(LoggedInConfig.urlPrefix).buildUpon()
    .append(NfUri.substitute(this)).build().toString()

fun String.toNfUri(withArgs: List<Pair<String, String>>): String =
    Uri.parse(LoggedInConfig.urlPrefix).buildUpon()
        .appendEncodedPath(NfUri.substitute(this))
        .apply { withArgs.forEach { (k, v) -> appendQueryParameter(k, v) } }.build().toString()