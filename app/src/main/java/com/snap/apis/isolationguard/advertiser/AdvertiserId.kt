package com.snap.apis.isolationguard.advertiser

import android.content.Context
import androidx.ads.identifier.AdvertisingIdClient
import com.snap.apis.isolationguard.utils.Threads
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

object AdvertiserId {
    fun determineAdvertisingInfoAsync(aContext: Context): Deferred<String?> {
        val context = aContext.applicationContext
        if (!AdvertisingIdClient.isAdvertisingIdProviderAvailable(context)) {
            return CompletableDeferred(null)
        }

        return Threads.withDeferred {
            AdvertisingIdClient.getAdvertisingIdInfo(context).get().id
        }
    }
}