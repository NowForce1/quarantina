package com.snap.apis.isolationguard.api

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.snap.apis.isolationguard.utils.DeviceStatus
import com.snap.apis.isolationguard.utils.TimeUtils
import com.snap.apis.isolationguard.utils.extensions.asSuccess
import com.snap.apis.isolationguard.utils.extensions.toNfUri
import org.json.JSONArray
import org.json.JSONObject

object LocationTransmitter {
    fun send(location: Location): HttpClient.Response {
        val retcode = LoggedInConfig.transceiver.post(
            "Locations/Update/json/#org/#org".toNfUri(),
            prepareSingleLocationJson(location).toString()
        )
        Log.v(LOG_TAG, "Send single - ${retcode.isSuccess.asSuccess}")

        return retcode
    }

    fun send(locations: List<Location>): HttpClient.Response {
        val bulk = JSONObject().put("LocationSetRequests", locations.fold(JSONArray(), { a, x -> a.put(prepareSingleLocationJson(x)) }))
        val retcode = LoggedInConfig.transceiver.post(
            "Locations/UpdateBulk/json/#org/#org".toNfUri(),
            bulk.toString()
        )
        Log.v(LOG_TAG, "Send multi (${locations.size}) - ${retcode.isSuccess.asSuccess}")

        return retcode
    }

    private fun prepareSingleLocationJson(location: Location): JSONObject {
        fun Location.isCell() = provider in listOf(LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER)
        val json = JSONObject().put("OrganizationId", LoggedInConfig.userDetails.orgId)
            .put("MobileUserId", LoggedInConfig.userDetails.userId).put("Latitude", location.latitude).put("Longtitude", location.longitude). // Please DON'T correct the misspelling
            put("IsCell", location.isCell()).put(
                "SecurityToken",
                LoggedInConfig.userDetails.securityToken
            ).put("DeviceId", LoggedInConfig.deviceId).put("BatteryLevel", DeviceStatus.lastBatteryLevel).put("FakeLoc", location.isFromMockProvider).put("RealTimeStamp", TimeUtils.secSince(location.time).coerceAtLeast(0)).put("AgeInSeconds", TimeUtils.secSince(location.time).coerceAtLeast(0))
        if (location.hasAccuracy()) {
            json.put("Accuracy", location.accuracy.toDouble())
        }

        if (location.hasBearing()) {
            json.put("Heading", location.bearing.toDouble())
        }

        if (location.hasSpeed()) {
            json.put("Speed", location.speed.toDouble())
        }

        Log.i(LOG_TAG, "Location age ${json.optLong("AgeInSeconds", -1)}")
        return json
    }

    private const val LOG_TAG: String = "LocationTransmitter"
}