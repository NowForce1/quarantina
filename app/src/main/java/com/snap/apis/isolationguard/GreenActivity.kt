package com.snap.apis.isolationguard

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.snap.apis.isolationguard.location.AlmanacEntry
import com.snap.apis.isolationguard.location.GreenLocationPolicyManager
import com.snap.apis.isolationguard.location.LocationStorageModel
import com.snap.apis.isolationguard.location.PathsAnalyser
import com.snap.apis.isolationguard.regionaldata.Almanac
import com.snap.apis.isolationguard.regionaldata.PublicConfig
import com.snap.apis.isolationguard.state.SystemState
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.*
import com.snap.apis.isolationguard.utils.extensions.orElse
import com.snap.apis.isolationguard.ux.relevantProvidersEnabled
import com.snap.apis.isolationguard.ux.requestFixLocations
import kotlinx.android.synthetic.main.activity_green.*
import kotlinx.android.synthetic.main.one_encounters_list_line.view.*
import java.util.*

class GreenActivity : AppCompatActivity() {
    private val permissionsLiveData = MutableLiveData<Boolean>()
    private val enableLocationLiveData = MutableLiveData<Boolean>()
    private val activityLogger by lazy { ActivityLogger("GreenActivity", lifecycle) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_green)

        activityLogger.monitor()
        val query: Map<String, String?> = intent?.data?.run { queryParameterNames.map { it to getQueryParameter(it) } }?.toMap() ?: mapOf()
        val command = intent?.data?.pathSegments?.firstOrNull()
        launchUI { handleArguments(command, query) }

        handleLocationRequests()

        trackLocationsSwitch.isChecked = (canGetLocations() && getUserWillForTracing()).also { enableLocationLiveData.postValue(it) }

        enableLocationLiveData.observe(this, Observer {
            activityLogger.log("Enable location: $it")
            SystemState.get(this).state = if (it == true) SystemState.State.GreenActive else SystemState.State.GreenSilent
            onEnableLocation(it == true)
        })

        val adapter = EncountersAdapter(this)
        encountersList.adapter = adapter

        PathsAnalyser.liveData.observe(this, Observer { data ->
            adapter.submitList(data ?: return@Observer)
            summaryText.text = if (data.isEmpty()) {
                getString(R.string.noEncountersFound)
            } else {
                getString(R.string.foundEncounters, data.size)
            }
        })

        registerBusyObservers()

        refreshAllButton.setOnClickListener {
            WorkerServiceCallBuilder.requestAlmanacRefresh(this).dispatch()
        }

        showMyLocationsButton.setOnClickListener { startActivity(Intent(this, DisplayLocationsActivity::class.java)) }
        WorkerServiceCallBuilder.initGreen(this@GreenActivity).dispatch()

    }

    private fun registerBusyObservers() {
        PublicConfig.statusLiveData.observe(this, Observer { status ->
            when (status) {
                is PublicConfig.DownloadStatus.Working -> {
                    busy(true)
                    infoLine = getString(R.string.downloadingData)
                }
                is PublicConfig.DownloadStatus.Success -> {
                    busy(false)
                    greenActivitySpinner.visibility = View.GONE
                    infoLine = ""
                }
                is PublicConfig.DownloadStatus.Failed -> {
                    busy(false)
                    activityLogger.log(status.message)
                    infoLine = getString(R.string.failedToDownloadConfig)
                }
                else -> {
                }
            }
        })

        Almanac.statusLiveData.observe(this, Observer { status ->
            when (status) {
                is Almanac.DownloadStatus.Working -> {
                    busy(true)
                    infoLine = getString(R.string.downloadingData)
                }
                is Almanac.DownloadStatus.Success -> {
                    busy(false)
                    greenActivitySpinner.visibility = View.GONE
                    infoLine = ""
                }
                is Almanac.DownloadStatus.Failed -> {
                    busy(false)
                    infoLine = getString(R.string.failedToDownloadAlmanac)
                }
                else -> {
                }
            }
        })

        PathsAnalyser.progressLiveData.observe(this, Observer { status ->
            when (status) {
                PathsAnalyser.Progress.IN_PROGRESS -> {
                    busy(true)
                    greenActivitySpinner.visibility = View.VISIBLE
                    infoLine = getString(R.string.analysingThatMayTakeAWhile)
                }
                PathsAnalyser.Progress.FINISHED -> {
                    busy(false)
                    greenActivitySpinner.visibility = View.GONE
                    infoLine = ""
                }
                else -> {
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        permissionsLiveData.postValue(hasLocationPermission())
    }

    private fun onEnableLocation(isChecked: Boolean) {
        if (!isChecked || canGetLocations()) {
            trackLocationsSwitch.setOnClickListener {
                val checked = trackLocationsSwitch.isChecked
                Prefs.write(this, LOCATIONS_USER_WILL, checked)
                enableLocationLiveData.postValue(checked)
            }
        } else if (!hasLocationPermission()) {
            trackLocationsSwitch.isChecked = false
            trackLocationsSwitch.setOnClickListener { requestPermissionsAsync() }
        }

        GreenLocationPolicyManager.get(this).startOrStop(isChecked && canGetLocations())

    }

    private fun handleLocationRequests() {
        permissionsLiveData.postValue(hasLocationPermission())
        val requestFixLocationsLd = MutableLiveData<Boolean>()
        val locationsEnabledObserver = Observer<Boolean> {
            val prevEnabledState = trackLocationsSwitch.isEnabled
            trackLocationsSwitch.isEnabled = canGetLocations().also {
                if (!prevEnabledState) {
                    trackLocationsSwitch.isChecked = getUserWillForTracing()
                }
            }
        }
        permissionsLiveData.observe(this, locationsEnabledObserver)
        requestFixLocationsLd.observe(this, locationsEnabledObserver)
        hasLocationPermission().orElse { requestPermissionsAsync() }
        relevantProvidersEnabled().orElse { requestFixLocations(requestFixLocationsLd) }
    }


    private fun getUserWillForTracing() = Prefs.read(this, LOCATIONS_USER_WILL, true)

    private fun canGetLocations() = hasLocationPermission() && relevantProvidersEnabled()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && hasLocationPermission()) {
            permissionsLiveData.postValue(true)
        }
    }

    private fun busy(busy: Boolean) {
        refreshAllButton.isEnabled = !busy
        refreshAllButton.alpha = if (busy) 0.3f else 1f
        greenActivitySpinner.visibility = if (busy) View.VISIBLE else View.GONE
    }

    private var infoLine: String? = ""
        set(string) {
            field = string
            greenInfoLine.text = string ?: ""
            greenInfoLine.visibility = if (string?.isNotEmpty() == true) View.VISIBLE else View.GONE
        }

    private suspend fun handleArguments(path: String?, query: Map<String, String?>) {
        val fromTs = TimeUtils.now
        val toTs = query["minutes"]?.toLong()?.min?.plus(fromTs.ms)?.ms
        val lat = query["lat"]?.toDouble()
        val lon = query["lon"]?.toDouble()
        val name = query["name"] ?: "=name="
        suspend fun createFakeUser() =
            LocationStorageModel().getDatabase(this).almanacDao().insert(AlmanacEntry(0xFACEC0DE, fromTs, toTs!!, lat!!, lon!!, "NOTE: Fake note", "Fake address", "Fake", name))
        when (path) {
            "fake_user" -> createFakeUser()
        }
    }

    private class EncountersAdapter(context: Context) : ListAdapter<PathsAnalyser.EncounterDescriptor, EncountersAdapter.VH>(object : DiffUtil.ItemCallback<PathsAnalyser.EncounterDescriptor>() {
        override fun areItemsTheSame(oldItem: PathsAnalyser.EncounterDescriptor, newItem: PathsAnalyser.EncounterDescriptor) = oldItem == newItem
        override fun areContentsTheSame(oldItem: PathsAnalyser.EncounterDescriptor, newItem: PathsAnalyser.EncounterDescriptor) = oldItem == newItem

    }) {
        class VH(view: View) : RecyclerView.ViewHolder(view) {
            val lineText: TextView = view.oneLocationLineText
            val lineTimeText: TextView = view.oneLocationLineTimeText
            val ctx: Context = view.context
        }

        private val dateFormat = DateFormat.getDateFormat(context)
        private val timeFormat = DateFormat.getTimeFormat(context)
        private fun ViewGroup.inflateChild(layout: Int) = LayoutInflater.from(context).inflate(layout, this, false)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH = VH(parent.inflateChild(R.layout.one_encounters_list_line))

        override fun onBindViewHolder(holder: VH, position: Int) {
            val item = getItem(position)
            val timeText: String = Date(item.startTime).let { "<b>${dateFormat.format(it)}</b><br/>${timeFormat.format(it)}" }
            holder.lineTimeText.text = HtmlCompat.fromHtml(timeText, HtmlCompat.FROM_HTML_MODE_LEGACY)
            holder.lineText.text = holder.ctx.getString(R.string.oneLineFormat, item.address, item.notes.let { if (it.isBlank()) "" else " ($it)" }, item.overallDuration.hms.run { "$hours:$minutes" })
            holder.lineText.textDirection = TextView.TEXT_DIRECTION_FIRST_STRONG
        }
    }

    override fun onStart() {
        super.onStart()
        val intent = Intent(this, WorkerService::class.java)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(serviceConnection)
    }

    private val serviceConnection by lazy { Connection() }

    private inner class Connection : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            activityLogger.log("Service $name disconnected")
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            activityLogger.log("Service $name connected")
        }

    }

    companion object {
        private const val LOCATIONS_USER_WILL = "LocationsUserWill"
    }

}
