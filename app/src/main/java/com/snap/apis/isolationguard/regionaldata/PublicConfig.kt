package com.snap.apis.isolationguard.regionaldata

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.snap.apis.isolationguard.api.HttpClient
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.*
import kotlin.properties.Delegates

object PublicConfig {
    private data class GreenPublicConfigFromApi(
        @SerializedName("overlap_time_in_min") val overlapTimeMin: Int,
        @SerializedName("overlap_distance_in_meters") val distanceMeters: Int,
        @SerializedName("location_sample_rate_in_minutes") val locationSampleRateMinutes: Float,
        @SerializedName("location_retention_days") val locationRetentionDays: Int,
        @SerializedName("quarantine_radius_in_meters") val quarantineRadiusMeters: Int,
        @SerializedName("location_refresh_minutes") val locationRefreshMinutes: Int?,
        @SerializedName("location_repository_url") val locationRepositoryUrl: String?
    )

    data class ConfigData(
        val overlapTime: Dt,
        val distanceMeters: Int,
        val locationSamplePeriod: Dt,
        val retentionPeriod: Dt,
        val quarantineRadiusMeters: Int,
        val almanacRefreshPeriod: Dt?,
        val locationRepositoryUrl: String?
    ) {
        fun toPrettyString() = listOf(
            Triple("Overlap time", overlapTime.hms.toString(), ""),
            Triple("Overlap distance", distanceMeters, "m"),
            Triple("Data retention", retentionPeriod.days, "days"),
            Triple("Location sample rate", locationSamplePeriod.sec, "sec"),
            Triple("Database update period", almanacRefreshPeriod?.hms?.toString() ?: "Not available", ""),
            Triple("Range for quarantined", quarantineRadiusMeters, "m")).joinToString("\n") { (title, number, unit) -> "${title.padEnd(30)}: $number$unit" }

    }

    private var configData: ConfigData? by Delegates.observable(null as ConfigData?) { _, old, new ->
        if (new != null && old != new) {
            Log.v(LOG_TAG, "Updated config to\n${new.toPrettyString()}")
            observable.call(new)
        }
    }
    fun download(context: Context): Boolean {
        statusLiveData.postValue(DownloadStatus.Working())
        val stopWatch = Stopwatch()
        val data = when (val retcode = HttpClient().get(CommonConsts.Uris.PUBLIC_CONFIG_URI, HttpClient.Headers())) {
            is HttpClient.SuccessHttpResponse -> retcode.result
            is HttpClient.FailedHttpResponse -> {
                statusLiveData.postValue(DownloadStatus.Failed(retcode.message))
                return false
            }
            else -> {
                statusLiveData.postValue(DownloadStatus.Failed("-N/A-"))
                return false
            }
        }
        configData = Gson().fromJson(data, GreenPublicConfigFromApi::class.java).let { c ->
            ConfigData(
                c.overlapTimeMin.min,
                c.distanceMeters,
                c.locationSampleRateMinutes.min,
                c.locationRetentionDays.days,
                c.quarantineRadiusMeters,
                c.locationRefreshMinutes?.min,
                c.locationRepositoryUrl)
        }

        Log.v(LOG_TAG, "Configuration was updated in $stopWatch sec")
        if (Prefs.read(context, CommonConsts.Debug.FAKE_CONFIG, false)) {
            configData = ConfigData(1.min, 200, 20.sec, 14.days, 100, 20.min, configData?.locationRepositoryUrl)
        }

        statusLiveData.postValue(DownloadStatus.Working())

        return true
    }

    fun isValid() = (configData?.locationSamplePeriod?.sec ?: -1) > 0

    fun get() = configData.takeIf { isValid() }

    val observable = Observable<ConfigData>()

    val statusLiveData = MutableLiveData<DownloadStatus>()

    sealed class DownloadStatus {
        class Working : DownloadStatus()
        class Success : DownloadStatus()
        class Failed(val message: String) : DownloadStatus()
    }
    private const val LOG_TAG = "PublicConfig"
}