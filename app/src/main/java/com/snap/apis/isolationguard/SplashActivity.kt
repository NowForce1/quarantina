package com.snap.apis.isolationguard

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.snap.apis.isolationguard.state.SystemState
import com.snap.apis.isolationguard.utils.ActivityLogger
import com.snap.apis.isolationguard.utils.delay
import com.snap.apis.isolationguard.utils.launchUI
import com.snap.apis.isolationguard.utils.sec
import com.snap.apis.isolationguard.ux.UxNavigator

class SplashActivity : AppCompatActivity() {
    private val activityLogger by lazy { ActivityLogger("SplashActivity", lifecycle) }
    private var alreadyLaunched = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        activityLogger.monitor()
        SystemState.get(this).stateLiveData.observe(this, Observer { state ->
            if (alreadyLaunched || state == SystemState.State.Unknown || state == null) return@Observer
            alreadyLaunched = true
            launchUI {
                val activityToLaunch = UxNavigator().getActivity(state)
                startActivity(Intent(this@SplashActivity, activityToLaunch).setData(intent?.data))
                finish()
            }
        })
        launchUI {
            delay(0.8.sec plusMinus 0.2.sec)
            with(SystemState.get(this@SplashActivity)) {
                if (state == SystemState.State.Unknown) {
                    state = SystemState.State.GreenSilent
                }
            }
        }
    }
}
