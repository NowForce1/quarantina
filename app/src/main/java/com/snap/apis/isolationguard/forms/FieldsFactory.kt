package com.snap.apis.isolationguard.forms

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.ViewGroup
import android.widget.RadioButton
import com.bumptech.glide.Glide
import com.snap.apis.isolationguard.R
import com.snap.apis.isolationguard.utils.Threads
import com.snap.apis.isolationguard.utils.extensions.addTextChangedLambda
import com.snap.apis.isolationguard.utils.extensions.checkedRadioButton
import com.snap.apis.isolationguard.utils.extensions.inflateChild
import com.snap.apis.isolationguard.utils.launchUI
import kotlinx.android.synthetic.main.fields_checkbox_line.view.*
import java.io.File
import java.util.concurrent.ConcurrentHashMap

enum class FieldType {
    TextBox,
    Label,
    Radio,
    Checkbox,
    Signature;

    companion object {
        fun valueOf(int: Int): FieldType = values()[int]
    }
}

class FieldsFactory {
    fun createForField(parent: ViewGroup, type: FieldType) = when (type) {
        FieldType.TextBox -> FieldViewHolder.Text(parent.inflateChild(R.layout.fields_text_line))
        FieldType.Label -> FieldViewHolder.Text(parent.inflateChild(R.layout.fields_label_line))
        FieldType.Radio -> FieldViewHolder.Radio(parent.inflateChild(R.layout.fields_radio_line))
        FieldType.Checkbox -> FieldViewHolder.Checkbox(parent.inflateChild(R.layout.fields_checkbox_line))
        FieldType.Signature -> FieldViewHolder.Signature(parent.inflateChild(R.layout.fields_signature))
    }

    fun bindItem(holder: FieldViewHolder<*>, item: FieldSpec<*>?) {
        when (item) {
            is FieldSpec.Text -> (holder as? FieldViewHolder.Text)?.run {
                title.text = item.caption
                text.inputType = item.flags
                text.setText(item.get)
                text.addTextChangedLambda { newText -> item.data.value = newText }
            }
            is FieldSpec.Label -> (holder as? FieldViewHolder.Label)?.run {
                title.text = item.caption
                text.text = item.get
            }
            is FieldSpec.Radio -> (holder as? FieldViewHolder.Radio)?.run {
                title.text = item.caption
                val initialChoice = item.data.value
                item.options.forEach { choice ->
                    group.addView((group.inflateChild(R.layout.field_radio_button) as RadioButton).also {
                        it.text = choice.name
                        it.tag = choice
                        it.isChecked = (initialChoice == choice)
                    })
                }
                group.setOnCheckedChangeListener { group, _ ->
                    item.data.value = group.checkedRadioButton()?.tag as? RadioOption ?: RadioOption("", "")
                }
            }
            is FieldSpec.Checkbox -> (holder as? FieldViewHolder.Checkbox)?.run {
                title.fieldCheckbox.text = item.caption
                checkbox.isChecked = item.get
                checkbox.setOnCheckedChangeListener { _, isChecked -> item.data.value = isChecked }
            }
            is FieldSpec.Signature -> (holder as? FieldViewHolder.Signature)?.run {
                title.text = item.caption
                launchUI {
                    val bitmap: Bitmap = persistentSignatures[item.fieldId]?.let { BitmapFactory.decodeByteArray(it, 0, it.size) }
                        ?: Threads.withBlocking { Glide.with(signatureView).asBitmap().submit().get() }
                    signatureView.setBitmap(bitmap)
                }
                acceptButton.setOnClickListener {
                    launchUI {
                        val byteArray = signatureView.capturePng()
                        item.data.value = File(acceptButton.context.filesDir, "sig.png").run { writeBytes(byteArray); absolutePath }
                    }
                }
                clearButton.setOnClickListener { signatureView.clear() }
            }
        }
    }

    private val persistentSignatures = ConcurrentHashMap<Int, ByteArray>()
}

val FieldSpec<*>.type: FieldType
    get() = when (this) {
        is FieldSpec.Text -> FieldType.TextBox
        is FieldSpec.Label -> FieldType.Label
        is FieldSpec.Radio -> FieldType.Radio
        is FieldSpec.Checkbox -> FieldType.Checkbox
        is FieldSpec.Signature -> FieldType.Signature
    }
