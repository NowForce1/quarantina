package com.snap.apis.isolationguard.api

import com.google.gson.annotations.SerializedName
import com.snap.apis.isolationguard.api.LocationPolicyData.SettingType.Companion.toSettingType

class LocationPolicyData {
    @Suppress("unused")
    enum class SettingType(val settingId: Int) {
        SILENT(0),
        ROUTINE_RESPONDER(1),
        ROUTINE_REPORTER(2),
        IN_SOS(3),
        EN_ROUTE(4),
        ON_SCENE(5),
        BEACONS_RATE(6),
        IN_ESCORT(7),
        INVALID(100);

        companion object {
            fun Int.toSettingType() = values().find { it.settingId == this } ?: INVALID
        }
    }

    data class LocationPolicy(@SerializedName("LocationSettingEntities") val entities: List<LocationPolicyEntity>) {
        data class LocationPolicyEntity(
            @SerializedName("Id") val id: Int,
            @SerializedName("SettingType") val setting: Setting,
            @SerializedName("Accuracy") val accuracy: Accuracy,
            @SerializedName("Rate") val rateSec: Float,
            @SerializedName("RateInMeters") val rateMeters: Float
        ) {
            data class Setting(@SerializedName("TypeId") val typeId: Int, @SerializedName("TypeName") val typeName: String)
            data class Accuracy(@SerializedName("AccuracyTypeId") val typeId: Int, @SerializedName("AccuracyLevelName") val levelName: String)

            val settingType get() = setting.typeId.toSettingType()
        }
    }


}
