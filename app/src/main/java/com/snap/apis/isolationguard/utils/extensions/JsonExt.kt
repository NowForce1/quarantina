package com.snap.apis.isolationguard.utils.extensions

import org.json.JSONArray
import org.json.JSONObject

fun <T : Any> JSONArray.mapNotNull(f: (JSONObject) -> T?): List<T> = (0 until length()).mapNotNull { i -> optJSONObject(i).let(f) }