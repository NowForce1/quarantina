package com.snap.apis.isolationguard.storage

import android.content.Context
import android.content.SharedPreferences

/**
 * Prefs wrapper
 *
 *
 * Created by shmuel on 27.7.15.
 */
@Suppress("unused")
object Prefs {
    private const val GENERIC_PREFS_FILE = "GenericPrefsFile"
    fun clean(context: Context?, key: String) {
        get(context)?.edit()?.remove(key)?.apply()
    }

    fun write(context: Context?, key: String, value: String) {
        get(context)?.edit()?.putString(key, value)?.apply()
    }

    fun write(context: Context?, key: String, value: Int) {
        get(context)?.edit()?.putInt(key, value)?.apply()
    }

    fun write(context: Context?, key: String, value: Long) {
        get(context)?.edit()?.putLong(key, value)?.apply()
    }

    fun write(context: Context?, key: String, value: Float) {
        get(context)?.edit()?.putFloat(key, value)?.apply()
    }

    fun write(context: Context?, key: String, value: Boolean) {
        get(context)?.edit()?.putBoolean(key, value)?.apply()
    }

    fun read(context: Context?, key: String, defaultValue: String): String =
        get(context)?.getString(key, defaultValue) ?: defaultValue

    fun read(context: Context?, key: String, defaultValue: Int): Int =
        get(context)?.getInt(key, defaultValue) ?: defaultValue

    fun read(context: Context?, key: String, defaultValue: Long): Long = try {
        get(context)?.getLong(key, defaultValue) ?: defaultValue
    } catch (ignored: Exception) {
        defaultValue
    }

    fun read(context: Context?, key: String, defaultValue: Float): Float = try {
        get(context)?.getFloat(key, defaultValue) ?: defaultValue
    } catch (ignored: Exception) {
        defaultValue
    }

    fun read(context: Context?, key: String, defaultValue: Boolean): Boolean =
        get(context)?.getBoolean(key, defaultValue) ?: defaultValue

    private fun get(context: Context?): SharedPreferences? {
        return (context ?: return null).getSharedPreferences(
            GENERIC_PREFS_FILE,
            Context.MODE_PRIVATE
        )
    }

    fun remove(context: Context?, key: String) {
        get(context)?.edit()?.remove(key)?.apply()
    }

    fun has(context: Context?, key: String): Boolean = get(context)?.contains(key) ?: false

    fun obliterateAll(context: Context?) {
        get(context)?.edit()?.clear()?.apply()
    }
}
