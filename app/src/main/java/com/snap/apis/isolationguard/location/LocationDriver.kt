package com.snap.apis.isolationguard.location

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.util.Log
import androidx.core.content.ContextCompat
import com.snap.apis.isolationguard.WorkerService
import com.snap.apis.isolationguard.utils.CommonConsts
import com.snap.apis.isolationguard.utils.Dt
import com.snap.apis.isolationguard.utils.hasLocationPermission
import java.util.concurrent.atomic.AtomicBoolean

class LocationDriver {
    data class LocationConfig(val dt: Dt, val dx: Float)

    class NoLocationService : RuntimeException("No location service")

    @SuppressLint("MissingPermission")
    fun start(context: Context, locationConfig: LocationConfig) {
        if (!context.hasLocationPermission()) return

        if (!isRunning.compareAndSet(false, true)) {
            return
        }
        val locationService = ContextCompat.getSystemService(context, LocationManager::class.java) ?: throw NoLocationService()
        stop(context)

        Log.v(LOG_TAG, "Starting the driver with dt=${locationConfig.dt.ms} dx=${locationConfig.dx}")
        locationService.allProviders.forEachIndexed { index, p ->
            locationService.requestLocationUpdates(p, locationConfig.dt.ms, locationConfig.dx, onLocationPendingIntent(context, index))
        }
    }

    companion object {
        private const val LOG_TAG = "LocationDriver"
        private val isRunning = AtomicBoolean(false)
        val isActive get() = isRunning.get()
        fun decodeIntent(intent: Intent): Location? = intent.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED)

        private fun onLocationPendingIntent(context: Context, id: Int): PendingIntent {
            val intent = Intent(context, WorkerService::class.java).setAction(CommonConsts.ON_LOCATION_ACTION)
            val startService = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) PendingIntent::getForegroundService else PendingIntent::getService
            return startService.invoke(context, id + 0x10c0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        fun stop(context: Context) {
            if (!isRunning.getAndSet(false)) { return }
            Log.v(LOG_TAG, "Stopping the locations driver")
            val locationService = ContextCompat.getSystemService(context, LocationManager::class.java) ?: return
            locationService.allProviders.forEachIndexed { index, _ ->
                locationService.removeUpdates(onLocationPendingIntent(context, index))
            }
        }
    }
}