package com.snap.apis.isolationguard.ux

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.snap.apis.isolationguard.R

fun AppCompatActivity.requestFixLocations(requestLiveData: MutableLiveData<Boolean>): Boolean {
    return if (!relevantProvidersEnabled()) {
        var userAgreedToFix = false
        AlertDialog.Builder(this).setTitle(R.string.warning).setMessage(R.string.locationServicesAreOff)
            .setPositiveButton(R.string.fixIt) { dialog, _ ->
                dialog.dismiss()
                userAgreedToFix = true
                lifecycle.addObserver(object : LifecycleObserver {
                    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
                    fun recheck() {
                        requestLiveData.postValue(relevantProvidersEnabled())
                    }
                })
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setOnDismissListener { requestLiveData.postValue(userAgreedToFix) }.show()
        false
    } else {
        requestLiveData.postValue(true)
        true
    }
}

fun AppCompatActivity.relevantProvidersEnabled(): Boolean =
    (getSystemService(Context.LOCATION_SERVICE) as? LocationManager)?.run { isProviderEnabled(LocationManager.GPS_PROVIDER) && isProviderEnabled(LocationManager.NETWORK_PROVIDER) } ?: false