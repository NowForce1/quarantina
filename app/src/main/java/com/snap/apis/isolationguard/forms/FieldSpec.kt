package com.snap.apis.isolationguard.forms

sealed class FieldSpec<T>(val fieldId: Int, val data: FieldResult<T>) {
    class Text(fieldId: Int, value: FieldResult<String>, val caption: String, val flags: Int) : FieldSpec<String>(fieldId, value)
    class Label(fieldId: Int, value: FieldResult<String>, val caption: String) : FieldSpec<String>(fieldId, value)
    class Checkbox(fieldId: Int, value: FieldResult<Boolean>, val caption: String) : FieldSpec<Boolean>(fieldId, value)
    class Radio(fieldId: Int, value: FieldResult<RadioOption>, val caption: String, val options: List<RadioOption>) : FieldSpec<RadioOption>(fieldId, value)
    class Signature(fieldId: Int, value: FieldResult<String>, val caption: String) : FieldSpec<String>(fieldId, value)

    val get get() = data.value
}

class RadioOption(val id: String, val name: String)

class FieldResult<T>(var value: T) {
    private val original = value
    val isDirty get() = original == value
}