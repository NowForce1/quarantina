package com.snap.apis.isolationguard.api

import androidx.annotation.WorkerThread
import com.google.gson.Gson
import com.snap.apis.isolationguard.utils.extensions.toNfUri
import java.util.*

object LocationPolicyFetcher {
    @WorkerThread
    fun fetch(): Map<LocationPolicyData.SettingType, List<LocationPolicyData.LocationPolicy.LocationPolicyEntity>> {
        val retcode =
            LoggedInConfig.transceiver.get("Locations/GetOrgLocationSettings/json/#org/#org".toNfUri())
        return if (retcode is HttpClient.SuccessHttpResponse) {
            val settings = Gson().fromJson(retcode.result, LocationPolicyData.LocationPolicy::class.java)
            settings.entities.groupBy { it.settingType }
        } else {
            EnumMap(LocationPolicyData.SettingType::class.java)
        }
    }
}