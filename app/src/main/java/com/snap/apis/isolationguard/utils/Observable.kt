package com.snap.apis.isolationguard.utils

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

class Observable<T> {
    private val repo = ConcurrentHashMap<Int, (T) -> Unit>()
    private val idCounter = AtomicInteger(0)
    fun register(f: (T) -> Unit) = idCounter.incrementAndGet().also { id -> repo[id] = f }
    fun unregister(id: Int) {
        repo.remove(id)
    }

    fun call(location: T) = repo.values.forEach { it.invoke(location) }
}

