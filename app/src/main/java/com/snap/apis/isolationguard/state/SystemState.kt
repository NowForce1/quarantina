package com.snap.apis.isolationguard.state

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.snap.apis.isolationguard.api.LoggedInConfig
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.Observable
import java.lang.ref.WeakReference
import kotlin.properties.Delegates

class SystemState(context: Context) {
    enum class State(val colour: Int) {
        Unknown(-1),
        GreenSilent(0),
        GreenActive(1),
        Yellow(2),
        Red(3);

        companion object {
            fun byOrdinal(i: Int) = values().getOrNull(i)
        }
    }

    fun init(): SystemState {
        contextRef.get()?.let { context -> detectSystemState(context) }
        return this
    }

    val stateLiveData = MutableLiveData<State>()
    var state: State by Delegates.observable(State.Unknown) { _, old, new ->
        if (new != State.Unknown && old != new) {
            Log.v(LOG_TAG, "State: $old -> $new")
            Prefs.write(context, ALERT_STATE_PREF_KEY, new.ordinal)
            stateObservable.call(old to new)
            stateLiveData.postValue(new)
        }
    }

    fun registerForState(f: (Pair<State, State>) -> Unit) = stateObservable.register(f)

    fun unregisterForState(id: Int) = stateObservable.unregister(id)
    private fun detectSystemState(context: Context): State {
        val resolution = if (state != State.Unknown) {
            state
        } else {
            val state = State.byOrdinal(Prefs.read(context, ALERT_STATE_PREF_KEY, State.Unknown.ordinal)) ?: State.Unknown
            when {
                state != State.Unknown -> state
                LoggedInConfig.isValid() && LoggedInConfig.load(context) -> State.Yellow
                !LoggedInConfig.isValid() && !LoggedInConfig.load(context) -> State.GreenActive
                else -> State.Unknown
            }
        }

        state = resolution
        return state
    }

    private val stateObservable = Observable<Pair<State, State>>()

    private val contextRef = WeakReference(context.applicationContext)
    val isGreen get() = state == State.GreenSilent || state == State.GreenActive
    companion object {
        private const val LOG_TAG = "SystemState"
        private const val ALERT_STATE_PREF_KEY = "AlertState"
        private var instance: SystemState? = null

        @Synchronized
        fun get(context: Context) = instance ?: SystemState(context).also { instance = it.init() }
    }

}