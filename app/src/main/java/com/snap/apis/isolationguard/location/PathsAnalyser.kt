package com.snap.apis.isolationguard.location

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.snap.apis.isolationguard.regionaldata.PublicConfig
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.*
import kotlinx.coroutines.runBlocking
import kotlin.math.cos

class PathsAnalyser(context: Context) {
    private val database = LocationStorageModel().getDatabase(context)

    init {
        Threads.submit { liveData.postValue(readLast(context)) }
    }

    data class EncounterDescriptor(val startTime: Long, val overallDuration: Dt, val lat: Double, val lon: Double, val address: String, val notes: String)

    fun analyse(context: Context): List<EncounterDescriptor> {
        val stopwatch = Stopwatch()
        progressLiveData.postValue(Progress.IN_PROGRESS)
        val perimeterSize = (PublicConfig.get()?.distanceMeters ?: 10).toDouble()

        val averageLat = runBlocking { database.locationsDao().getAverageLat() } ?: let {
            return finishAnalysis(context, 0.0, listOf())
        }
        val earthPerimeter = 40e6
        val latTolerance = perimeterSize * (360.0 / earthPerimeter)

        // The perimeter of each parallel = perimeter at the equator * cos (lat)
        val cosLat = cos(averageLat.deg)
        val lonTolerance = if (cosLat > 0) perimeterSize * (360.0 / (earthPerimeter * cosLat)) else 0.0

        Log.v(LOG_TAG, "For ${perimeterSize}m: dLat=$latTolerance dLon=$lonTolerance")

        val exposureTime = PublicConfig.get()?.overlapTime ?: 10.min
        val sampleRate = PublicConfig.get()?.locationSamplePeriod ?: 30.min
        val encountersMap = runBlocking { database.encountersDao().findEncounters(latTolerance, lonTolerance).groupBy { it.almanacId } }
        val encounters = mutableListOf<EncounterDescriptor>()
        for (incidentLog in encountersMap.values) {
            val first = incidentLog.firstOrNull() ?: continue
            var sumTime = Dt(0)
            var prevTs = first.ts
            for (item in incidentLog) {
                val deltaFromPrev = (item.ts - prevTs).ms
                if (deltaFromPrev < sampleRate * 4) {
                    sumTime += deltaFromPrev
                }
                prevTs = item.ts
            }

            if (sumTime > exposureTime) {
                encounters.add(EncounterDescriptor(first.ts, sumTime, first.lat, first.lon, first.address, first.notes))
            }
        }

        val elapsed = stopwatch.lapSec
        return finishAnalysis(context, elapsed, encounters)
    }

    private fun finishAnalysis(context: Context, elapsed: Double, encounters: List<EncounterDescriptor>): List<EncounterDescriptor> {
        progressLiveData.postValue(Progress.FINISHED)

        Log.v(LOG_TAG, "Finished analysis with ${encounters.size} results in $elapsed sec")

        return encounters.also {
            liveData.postValue(it)
            store(context, it)
        }
    }

    private data class EncList(val data: List<EncounterDescriptor>)

    private fun store(context: Context, list: List<EncounterDescriptor>) {
        Prefs.write(context, "Encounters", Gson().toJson(EncList(list)))
    }

    enum class Progress { IDLE, IN_PROGRESS, FINISHED }
    companion object {
        private const val LOG_TAG = "PathAnalyser"
        val progressLiveData = MutableLiveData<Progress>().apply { postValue(Progress.IDLE) }
        val liveData = MutableLiveData<List<EncounterDescriptor>>()
        fun readLast(context: Context): List<EncounterDescriptor> = Prefs.read(context, "Encounters", Gson().toJson(EncList(listOf()))).let { Gson().fromJson(it, EncList::class.java).data }
    }
}