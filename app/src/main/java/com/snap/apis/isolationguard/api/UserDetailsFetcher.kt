package com.snap.apis.isolationguard.api

import com.google.gson.Gson
import com.snap.apis.isolationguard.utils.extensions.toNfUri

object UserDetailsFetcher {
    private val nullUserDetails = UserDetails("", -1, -1)
    fun fetch(): UserDetails {
        val result = (LoggedInConfig.transceiver.post(
            "user/QueryUsers/json/#org/#org".toNfUri(),
            Gson().toJson(mapOf("UserId" to LoggedInConfig.userDetails.userId))
        ) as? HttpClient.SuccessHttpResponse)?.result ?: return nullUserDetails
        return Gson().fromJson(result, Array<UserDetails>::class.java)?.firstOrNull() ?: nullUserDetails
    }
}