package com.snap.apis.isolationguard.utils

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

/**
 * Description:
 * Created by Theodor on 8.4.15.
 */
object DeviceStatus {
    var lastBatteryLevel: Int = -1
        private set

    fun sampleBatteryLevel(context: Context?): Int {
        context ?: return -1
        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryStatus = context.applicationContext.registerReceiver(null, intentFilter) ?: return -1
        val level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

        lastBatteryLevel = level * 100 / scale
        return lastBatteryLevel
    }
}
