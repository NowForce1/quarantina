package com.snap.apis.isolationguard.location

import android.content.Context
import androidx.room.*

@Entity(tableName = "Locations", indices = [Index(value = ["ts", "lat", "lon"])])
data class LocationEntry(@PrimaryKey(autoGenerate = true) val id: Long, val ts: Long, val lat: Double, val lon: Double, val provider: String, val accuracy: Float, val outsidePerimeter: Boolean, val stateColour: Int)

@Entity(tableName = "LocationSending", indices = [Index(value = ["ts"])])
data class LocationSentEntry(@PrimaryKey(autoGenerate = true) val id: Long, val ts: Long, val firstLocationTs: Long, val quantity: Int, val success: Boolean)

@Entity(tableName = "Almanac", indices = [Index(value = ["fromTs", "toTs", "lat", "lon"])])
data class AlmanacEntry(@PrimaryKey(autoGenerate = true) val id: Long, val fromTs: Long, val toTs: Long, val lat: Double, val lon: Double, val notes: String, val address: String, val locationType: String, val name: String)

@Entity(tableName = "Encounters")
data class Encounters(@PrimaryKey val almanacId: Long, val ts: Long, val lat: Double, val lon: Double, val notes: String, val address: String)

@Dao
interface LocationsDao {
    @Insert
    suspend fun insert(locationEntry: LocationEntry)

    @Query("delete from Locations where Locations.ts < :refTime ")
    suspend fun deleteAnythingBefore(refTime: Long)

    @Query("select * from Locations where ts > :timeStamp ORDER BY ts DESC")
    suspend fun getLocationsSince(timeStamp: Long): List<LocationEntry>

    @Query("select avg(lat) from Locations")
    suspend fun getAverageLat(): Double?
}

@Dao
interface LocationSendDao {
    @Insert
    suspend fun insert(locationSendEntry: LocationSentEntry)

    @Query("delete from LocationSending where ts < :ts")
    suspend fun sanitise(ts: Long)

    @Query("select ts from LocationSending where success=1 order by ts DESC LIMIT 1")
    suspend fun getLastSuccessfulSendTime(): Long?
}

@Dao
interface AlmanacDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(almanacData: List<AlmanacEntry>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: AlmanacEntry)

    @Query("delete from Almanac where Almanac.toTs<:ts")
    suspend fun deleteUpToTs(ts: Long)

    @Transaction
    suspend fun update(deleteByTs: Long, almanacData: List<AlmanacEntry>) {
        deleteUpToTs(deleteByTs)
        insertAll(almanacData)
    }
}

@Dao
interface EncountersDao {
    @Query("""SELECT Almanac.id as almanacId, Locations.ts, Locations.lat, Locations.lon, Almanac.notes, Almanac.address from Locations join Almanac where
            (Locations.ts between Almanac.fromTs and Almanac.toTs)
            and (Locations.lat between (Almanac.lat - :latTolerance) and (Almanac.lat + :latTolerance)) 
            and (Locations.lon between (Almanac.lon - :lonTolerance) and (Almanac.lon + :lonTolerance))
            limit 1000""")
    suspend fun findEncounters(latTolerance: Double, lonTolerance: Double): List<Encounters>
}

@Database(entities = [LocationEntry::class, LocationSentEntry::class, AlmanacEntry::class], version = 1)
abstract class LocationsDatabase : RoomDatabase() {
    abstract fun locationsDao(): LocationsDao
    abstract fun lastLocationSentDao(): LocationSendDao
    abstract fun almanacDao(): AlmanacDao
    abstract fun encountersDao(): EncountersDao
}

class LocationStorageModel {
    @Synchronized
    fun getDatabase(context: Context) = database ?: Room.databaseBuilder(context.applicationContext, LocationsDatabase::class.java, "Locations.db")
        .fallbackToDestructiveMigration()
        .build().also { database = it }

    companion object {
        private var database: LocationsDatabase? = null
    }
}
