package com.snap.apis.isolationguard.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

/**
 * Description: Missing methods in Continuation
 * Created by Theodor on 8.8.18.
 */
/**
 * Resume - with no crashes
 * @param t the return value to resume
 *
 * @return a string with the exception description, if any
 */
fun <T> Continuation<T>.safeResume(t: T): String? = try {
    resume(t)
    null
} catch (e: Exception) {
    e.toString()
}

fun launchUI(f: suspend CoroutineScope.() -> Unit) = GlobalScope.launch(Dispatchers.Main, block = {
    try {
        f()
    } catch (ignored: Exception) {
        ignored.printStackTrace()
    }
})
