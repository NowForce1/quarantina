package com.snap.apis.isolationguard.api

import android.util.Log
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody

/**
 * Description:
 * Created by shmuel on 2019-05-30.
 */
open class HttpClient {
    data class Headers(val pairs: List<Pair<String, String>>) {
        constructor(vararg pairs: Pair<String, String>) : this(pairs.toList())
    }

    data class Body(val body: String, val mime: String)

    @Suppress("unused")
    abstract class Response(val isSuccess: Boolean)
    data class SuccessHttpResponse(val result: String) : Response(true)
    data class FailedHttpResponse(val code: Int, val message: String) : Response(false)

    private val onFaultyResponse: (Response) -> Unit = {
        Log.v("HttpClient", "Failing with ${when (it) {
            is SuccessHttpResponse -> "--Success code--"
            is FailedHttpResponse -> "err code ${it.code} and message ${it.message}"
            else -> "Unclear..."
        }}")
    }
    fun post(url: String, body: Body, headers: Headers): Response {
        val refTime = System.currentTimeMillis()
        val request = Request.Builder().url(url).apply {
            headers.pairs.forEach { (k, v) -> addHeader(k, v) }
        }.post(body.body.toRequestBody(body.mime.toMediaType())).build()

        val result = try {
            client.newCall(request).execute()
        } catch (e: Exception) {
            return FailedHttpResponse(-1, e.toString()).apply { Log.v("HTTP", toString()) }
        }

        return result.let { r ->
            if (r.isSuccessful) {
                SuccessHttpResponse(r.body?.string() ?: "").also { closeTheResponse(r) }
            } else {
                FailedHttpResponse(r.code, r.message).also { fault ->
                    onFaultyResponse.invoke(fault)
                    closeTheResponse(r)
                }
            }
        }.also { println("$url took ${System.currentTimeMillis() - refTime}") }
    }

    fun get(url: String, headers: Headers): Response {
        val refTime = System.currentTimeMillis()
        val request = Request.Builder().url(url).apply {
            headers.pairs.forEach { (k, v) -> addHeader(k, v) }
        }.get().build()

        val result = try {
            client.newCall(request).execute()
        } catch (e: Exception) {
            return FailedHttpResponse(-1, e.toString()).apply { Log.v("HTTP", toString()) }
        }

        return result.let { r ->
            if (r.isSuccessful) {
                SuccessHttpResponse(r.body?.string() ?: "").also { closeTheResponse(r) }
            } else {
                FailedHttpResponse(r.code, r.message).also { fault ->
                    onFaultyResponse.invoke(fault)
                    closeTheResponse(r)
                }
            }
        }.also { println("$url took ${System.currentTimeMillis() - refTime}") }
    }

    private fun closeTheResponse(result: okhttp3.Response) {
        try {
            result.body?.close()
        } catch (ignored: Exception) {
        }
    }

    companion object {
        private val client: OkHttpClient by lazy { OkHttpClient() }
    }
}