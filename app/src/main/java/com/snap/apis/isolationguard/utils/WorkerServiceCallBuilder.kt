package com.snap.apis.isolationguard.utils

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import com.snap.apis.isolationguard.WorkerService

class WorkerServiceCallBuilder(val context: Context, action: String) {
    val intent = Intent(context, WorkerService::class.java).setAction(action)
    fun dispatch() = ContextCompat.startForegroundService(context, intent)

    companion object {
        fun scheduleAlmanacRefresh(context: Context, dt: Dt) = WorkerServiceCallBuilder(context, CommonConsts.SCHEDULE_REFRESH_ALMANAC_ACTION).also {
            it.intent.putExtra(CommonConsts.SCHEDULE_REFRESH_ALMANAC_TIME_EXTRA, dt)
        }

        fun requestAlmanacRefresh(context: Context) = WorkerServiceCallBuilder(context, CommonConsts.REFRESH_ALMANAC_ACTION)

        fun initGreen(context: Context) = WorkerServiceCallBuilder(context, CommonConsts.INIT_GREEN_ACTION)
    }
}
