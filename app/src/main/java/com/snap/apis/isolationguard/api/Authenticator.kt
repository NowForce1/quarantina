package com.snap.apis.isolationguard.api

import android.util.Base64
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.annotations.SerializedName
import com.snap.apis.isolationguard.utils.Threads

/**
 * Description:
 * Created by shmuel on 2019-05-30.
 */
class Authenticator private constructor() {
    private val gson by lazy { Gson() }
    private fun auth(username: String, password: String, serverUrl: String): AuthResult {
        val body = "grant_type=password2&app_type=mobileApp"
        val headerToken = base64("$username;$password")

        val result = HttpClient().post(
            "https://$serverUrl/api/en-us/OAuth/AccessToken",
            HttpClient.Body(body, "application/x-www-form-urlencoded"),
            HttpClient.Headers("Authorization" to "Basic $headerToken")
        )

        return if (result is HttpClient.SuccessHttpResponse) {
            val success = JsonParser.parseString(result.result)?.asJsonObject?.get("Error")?.takeIf { it is JsonObject } == null
            if (success) {
                gson.fromJson(result.result, AuthResult.AuthSuccess::class.java)
            } else {
                gson.fromJson(result.result, AuthResult.AuthFailure::class.java)
            }
        } else AuthResult.AuthFailure(null)
    }

    private fun login(auth: AuthResult.AuthSuccess, appData: AppData): LoginResult {
        val nfHttpClient = NFHttpClient(auth.accessToken)
        val result = nfHttpClient.post(
            "https://${auth.serverUrl}/api/en-us/User/Login/json/-1/-1",
            gson.toJson(
                mapOf(
                    "AdvertiserId" to appData.advertiserId?.takeIf { it.isNotBlank() },
                    "DeviceId" to "Chariton-${appData.deviceId}",
                    "AppType" to 1,
                    "InstalledVersion" to appData.versionNumber
                )
            )
        )

        return when (result) {
            is HttpClient.SuccessHttpResponse -> {
                val userDetails = gson.fromJson(result.result, UserDetailsResponse::class.java).MobileUser
                LoginResult.LoginSuccess(auth.serverUrl ?: "", nfHttpClient, userDetails)
            }
            is HttpClient.FailedHttpResponse -> {
                LoginResult.LoginFault(result.message)
            }
            else -> LoginResult.LoginFault("N/A")
        }
    }

    private fun base64(from: String): String {
        val byteArray = Base64.encode(if (from.isNotEmpty()) from.toByteArray() else byteArrayOf(), Base64.NO_WRAP)
        return String(byteArray)
    }

    sealed class AuthResult {
        data class AuthSuccess(
            @SerializedName("access_token") val accessToken: String,
            @SerializedName("LoginUrl") val serverUrl: String?,
            @SerializedName("IsLoggedin") val isLoggedIn: Boolean
        ) : AuthResult()

        data class AuthFailure(@SerializedName("Error") val authError: AuthError?) : AuthResult() {
            data class AuthError(@SerializedName("LoginErrorType") val errorType: Int)
        }
    }

    sealed class LoginResult {
        data class LoginSuccess(val serverPrefix: String, val transceiver: NFHttpClient, val userDetails: UserDetails) : LoginResult() {
            val isValid = userDetails.userId > 0
        }

        data class LoginFault(val comment: String) : LoginResult()
    }

    data class AppData(val versionNumber: String, var deviceId: String, val advertiserId: String?)

    companion object {
        suspend fun authenticate(username: String, password: String, serverUrl: String) =
            Threads.withBlocking { Authenticator().run { auth(username, password, serverUrl) } }

        suspend fun login(auth: AuthResult.AuthSuccess, appData: AppData) =
            Threads.withBlocking { Authenticator().login(auth, appData) }
    }
}