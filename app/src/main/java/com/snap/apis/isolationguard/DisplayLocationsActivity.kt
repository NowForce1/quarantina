package com.snap.apis.isolationguard

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.snap.apis.isolationguard.location.LocationEntry
import com.snap.apis.isolationguard.location.LocationStorageModel
import com.snap.apis.isolationguard.utils.Dt
import com.snap.apis.isolationguard.utils.days
import com.snap.apis.isolationguard.utils.launchUI
import kotlinx.android.synthetic.main.activity_display_locations.*
import kotlinx.android.synthetic.main.one_encounters_list_line.view.oneLocationLineText
import kotlinx.android.synthetic.main.one_encounters_list_line.view.oneLocationLineTimeText
import kotlinx.android.synthetic.main.one_locations_list_line.view.*
import java.util.*
import java.util.concurrent.ConcurrentHashMap


class DisplayLocationsActivity : AppCompatActivity() {
    private val viewModel: LocationsListViewModel by lazy { VMFactory(this).create(LocationsListViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_locations)
        val adapter = LocationAdapter(this).also { locationListContainer.adapter = it }
        viewModel.liveData.observe(this, Observer { l ->
            l?.let { adapter.submitList(l) }
        })
        refreshLocations()

        refreshLocationsListFab.setOnClickListener { refreshLocations() }
    }

    private fun refreshLocations() {
        launchUI {
            viewModel.getLocations(14.days.ago)
        }
    }

    private class VMFactory(val context: Context) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java).newInstance(context)
        }
    }

    private class LocationAdapter(activity: DisplayLocationsActivity) : ListAdapter<LocationsListViewModel.Place, LocationAdapter.Holder>(object : DiffUtil.ItemCallback<LocationsListViewModel.Place>() {
        override fun areItemsTheSame(oldItem: LocationsListViewModel.Place, newItem: LocationsListViewModel.Place): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: LocationsListViewModel.Place, newItem: LocationsListViewModel.Place): Boolean =
            oldItem.locationItem.id == newItem.locationItem.id && oldItem.placeName == newItem.placeName
    }) {
        private class Holder(val view: View) : RecyclerView.ViewHolder(view) {
            val lineText: TextView = view.oneLocationLineText
            val lineTimeText: TextView = view.oneLocationLineTimeText
            val providerIcon: ImageView = view.oneLocationLineNetworkSourceIndicator
        }

        private val layoutInflater = activity.layoutInflater
        private val dateFormat = DateFormat.getDateFormat(activity)
        private val timeFormat = DateFormat.getTimeFormat(activity)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder = Holder(layoutInflater.inflate(R.layout.one_locations_list_line, parent, false))
        override fun onBindViewHolder(holder: Holder, position: Int) {
            val item = getItem(position)
            val timeText: String = Date(item.locationItem.ts).let { "<b>${dateFormat.format(it)}</b><br/>${timeFormat.format(it)}" }
            holder.lineText.text = item.run { "${locationItem.lat},${locationItem.lon} $placeName" }
            holder.lineTimeText.text = HtmlCompat.fromHtml(timeText, HtmlCompat.FROM_HTML_MODE_LEGACY)
            holder.providerIcon.setImageResource(when (item.locationItem.provider.toLowerCase(Locale.ENGLISH)) {
                "network" -> R.drawable.ic_wifi_black_24dp
                "gps" -> R.drawable.ic_gps_fixed_black_24dp
                "passive" -> R.drawable.ic_map_black_24dp
                else -> R.drawable.ic_location_black_24dp
            })
            holder.view.setOnClickListener { v ->
                val gmmIntentUri: Uri = item.locationItem.run { Uri.parse("geo:$lat,$lon") }
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                v.context.startActivity(mapIntent)
            }
        }
    }
}

private class LocationsListViewModel(val context: Context) : ViewModel() {
    class Place(val locationItem: LocationEntry, var placeName: String)
    val liveData = MutableLiveData<List<Place>>()
    private val database = LocationStorageModel().getDatabase(context)
    private val places = ConcurrentHashMap<String, String>()
    private fun LocationEntry.hashLatLon() = "${(lat * 10000).toInt()},${(lon * 10000).toInt()}"
    suspend fun getLocations(since: Dt) = database.locationsDao().getLocationsSince(since.ms).map { Place(it, places[it.hashLatLon()] ?: "") }.also { liveData.postValue(it) }
}
