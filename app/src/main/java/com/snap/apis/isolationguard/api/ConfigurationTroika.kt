package com.snap.apis.isolationguard.api

import com.google.gson.annotations.SerializedName

data class GeneralConfigEntry(@SerializedName("Key") val key: String, @SerializedName("Value") val value: String)
data class OrgConfigEntry(@SerializedName("ConfigurationId") val id: Int, @SerializedName("ConfigurationValue") val value: String)
data class CtdEntry(
    @SerializedName("CodeId") val codeId: Int,
    @SerializedName("CodeTableId") val ctdId: Int,
    @SerializedName("IconUrl") val iconUrl: String,
    @SerializedName("BusinessRule1") val businessRule1: String
)

@Suppress("ArrayInDataClass")
data class ConfigurationTroika(
    @SerializedName("General") val general: Array<GeneralConfigEntry>,
    @SerializedName("OrganizationConfigurations") val orgConfigs: Array<OrgConfigEntry>,
    @SerializedName("CodeTableDataItems") val ctd: Array<CtdEntry>
)
