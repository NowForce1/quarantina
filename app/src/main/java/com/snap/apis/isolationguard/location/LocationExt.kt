package com.snap.apis.isolationguard.location

import android.location.Location

fun Location.toEntry(isOutsidePerimeter: Boolean, stateColour: Int) =
    LocationEntry(
        0,
        time,
        latitude,
        longitude,
        provider,
        accuracy,
        isOutsidePerimeter,
        stateColour
    )

