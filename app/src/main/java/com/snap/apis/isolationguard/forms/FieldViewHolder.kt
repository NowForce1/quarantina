package com.snap.apis.isolationguard.forms

import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fields_checkbox_line.view.*
import kotlinx.android.synthetic.main.fields_radio_line.view.*
import kotlinx.android.synthetic.main.fields_signature.view.*
import kotlinx.android.synthetic.main.fields_text_line.view.*

sealed class FieldViewHolder<T>(view: View, val title: TextView) : RecyclerView.ViewHolder(view) {
    class Text(view: View) : FieldViewHolder<String>(view, view.textFieldCaption) {
        val text: EditText = view.textFieldEditText
    }

    class Label(view: View) : FieldViewHolder<String>(view, view.textFieldCaption) {
        val text: TextView = view.textFieldEditText
    }

    class Radio(view: View) : FieldViewHolder<RadioOption>(view, view.fieldRadioLabel) {
        val group: RadioGroup = view.fieldRadio
    }

    class Checkbox(view: View) : FieldViewHolder<Boolean>(view, view.fieldCheckbox) {
        val checkbox: CheckBox = view.fieldCheckbox
    }

    class Signature(view: View) : FieldViewHolder<ByteArray>(view, view.fieldSignatureLabel) {
        val signatureView = view.doodleView
        val acceptButton = view.acceptSignatureButton
        val clearButton = view.clearSignatureButton
    }
}