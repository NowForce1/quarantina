package com.snap.apis.isolationguard.utils.lifecycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(private vararg val arguments: Any) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val classesArray: Array<Class<out Any>> = arguments.map { it::class.java }.toTypedArray()
        return modelClass.getConstructor(*classesArray).newInstance(*arguments)
    }
}