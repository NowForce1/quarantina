package com.snap.apis.isolationguard.utils

class Stopwatch {
    private val refTime = System.nanoTime()
    private val lap get() = (System.nanoTime() - refTime)
    val lapSec get() = lap.toDouble() / 1e9
    override fun toString(): String = lapSec.toString()
}