package com.snap.apis.isolationguard.api

/**
 * Description:
 * Created by shmuel on 2019-05-30.
 */
class NFHttpClient(val token: String) : HttpClient() {
    fun post(url: String, body: String): Response = super.post(url, Body(body, JSON), Headers("Authorization" to "Bearer $token")).also { monitor(it) }
    fun get(url: String): Response = super.get(url, Headers("Authorization" to "Bearer $token")).also { monitor(it) }

    @Suppress("unused")
    val isValid
        get() = token.isNotBlank()

    private fun monitor(response: Response) {
        if (response is FailedHttpResponse && response.code == 403) {
            LoggedInConfig.invalidate()
        }
    }

    companion object {
        private const val JSON = "application/json; charset=utf-8"
    }
}
