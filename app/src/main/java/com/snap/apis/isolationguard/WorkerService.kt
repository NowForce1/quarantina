package com.snap.apis.isolationguard

import android.app.AlarmManager
import android.app.IntentService
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.AlarmManagerCompat
import com.snap.apis.isolationguard.api.ConfigurationTroikaFetcher
import com.snap.apis.isolationguard.api.LocationPolicyFetcher
import com.snap.apis.isolationguard.api.LoggedInConfig
import com.snap.apis.isolationguard.api.UserDetailsFetcher
import com.snap.apis.isolationguard.location.GreenLocationPolicyManager
import com.snap.apis.isolationguard.location.LocationPolicyManager
import com.snap.apis.isolationguard.location.PathsAnalyser
import com.snap.apis.isolationguard.location.RegisteredUserLocationPolicyManager
import com.snap.apis.isolationguard.regionaldata.Almanac
import com.snap.apis.isolationguard.regionaldata.PublicConfig
import com.snap.apis.isolationguard.state.SystemState
import com.snap.apis.isolationguard.utils.*
import com.snap.apis.isolationguard.utils.extensions.sumHash
import kotlinx.coroutines.CompletableDeferred
import java.util.concurrent.atomic.AtomicBoolean


/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
class WorkerService : Service() {
    private val almanac by lazy { Almanac.get(this) }

    private suspend fun WorkerService.executeIntent(intent: Intent?) {
        val action = intent?.action ?: if (SystemState.get(this@WorkerService).isGreen) CommonConsts.INIT_GREEN_ACTION else CommonConsts.INIT_YELLOW_ACTION
        if (action in oneShotActions && !executedActions.add(action)) return
        autoInit()
        when (action) {
            CommonConsts.INIT_GREEN_ACTION -> setupGreen()
            CommonConsts.INIT_YELLOW_ACTION -> setupYellow(intent?.getBooleanExtra(CommonConsts.FIRST_INIT_EXTRA, false) ?: false)
            CommonConsts.ON_LOCATION_ACTION -> locationPolicyManager.onLocation(intent ?: return)
            CommonConsts.INVALID_CONFIG_ACTION -> startActivity(Intent(this@WorkerService, LoginActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK))
            CommonConsts.REFRESH_ALMANAC_ACTION -> refreshAllPublicDataIfRequired()
            CommonConsts.SCHEDULE_REFRESH_ALMANAC_ACTION -> scheduleRefreshPublicData(intent?.getParcelableExtra(CommonConsts.SCHEDULE_REFRESH_ALMANAC_TIME_EXTRA))
        }
    }

    private suspend fun autoInit() {
        val state = SystemState.get(this).state
        if (SystemState.get(this).isGreen) {
            setupGreen()
        } else if (state == SystemState.State.Yellow && !LoggedInConfig.isValid()) {
            setupYellow(false)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForeground(0xF9, Notifications(this@WorkerService).foregroundNotification())
        launchUI {
            val stopwatch = Stopwatch()
            executeIntent(intent)
            Log.v(LOG_TAG, "Processing action ${intent?.action} took $stopwatch sec")
        }
        return START_STICKY
    }

    private suspend fun setupGreen() {
        if (!greenInitExecuted.compareAndSet(false, true)) return
        Log.v(LOG_TAG, "Setup green, v.${packageManager?.getPackageInfo(packageName, 0)?.versionName}")
        refreshAllPublicDataIfRequired()

        if (hasLocationPermission()) {
            locationPolicyManager.setup(this)
        }

        scheduleRefreshPublicData()
    }

    private suspend fun refreshAllPublicDataIfRequired() {
        val context = this@WorkerService
        val configurationDownloadSuccess = Threads.withBlocking { PublicConfig.download(this) }
        scheduleRefreshPublicData()
        val success = if (PublicConfig.isValid() && configurationDownloadSuccess) {
            Threads.withBlocking { almanac.downloadAlmanac(context) }
        } else {
            scheduleRefreshPublicData(1.min plusMinus 20.sec)
            false
        }

        if (success || !almanac.isOlderThan(PublicConfig.get()?.almanacRefreshPeriod ?: 3.hours)) {
            val encounters = Threads.withBlocking { PathsAnalyser(context).analyse(context) }
            if (encounters.isNotEmpty()) {
                Notifications(context).notifyNewData()
            }
        }
    }

    private fun scheduleRefreshPublicData(duration: Dt? = null) {
        val refreshPeriod: Dt = duration ?: PublicConfig.get()?.almanacRefreshPeriod ?: 3.hours
        Log.v(LOG_TAG, "Refresh period is set to ${refreshPeriod.fromNow.asDate}")
        val scheduleTime = TimeUtils.now + refreshPeriod.ms
        val alarmManager = (getSystemService(Context.ALARM_SERVICE) as AlarmManager)
        val fPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) PendingIntent::getForegroundService else PendingIntent::getService
        val pendingIntent = fPendingIntent(this, "ALMC".sumHash, Intent(this, WorkerService::class.java).setAction(CommonConsts.REFRESH_ALMANAC_ACTION), PendingIntent.FLAG_UPDATE_CURRENT)
        AlarmManagerCompat.setAndAllowWhileIdle(alarmManager, AlarmManager.RTC_WAKEUP, scheduleTime, pendingIntent)
    }

    private suspend fun setupYellow(isFirstInit: Boolean): Boolean {
        if (!yellowInitExecuted.compareAndSet(false, true)) return true
        val deferred = CompletableDeferred<Boolean>()
        if (isFirstInit) {
            LoggedInConfig.save(this)
        }
        RegisteredUserLocationPolicyManager.stop(this)
        Threads.submit {
            if (!reloadConfig()) return@submit
            LoggedInConfig.userDetails = if (LoggedInConfig.isValid()) LoggedInConfig.userDetails else UserDetailsFetcher.fetch()
            LoggedInConfig.troika = if (LoggedInConfig.troika?.orgConfigs?.isNotEmpty() == true) LoggedInConfig.troika else ConfigurationTroikaFetcher.fetch()
            LoggedInConfig.locations = if (LoggedInConfig.locations?.isNotEmpty() == true) LoggedInConfig.locations else LocationPolicyFetcher.fetch()
            LoggedInConfig.save(this)
            RegisteredUserLocationPolicyManager.get(this).apply {
                applyConfig()
                applyBoundary()
            }

            deferred.complete(true)
        }

        return deferred.await()
    }

    private fun reloadConfig(): Boolean {
        if (!LoggedInConfig.isValid()) {
            LoggedInConfig.load(this)
        }
        return LoggedInConfig.isValid()
    }

    private val locationPolicyManager: LocationPolicyManager
        get() = when (SystemState.get(this).state) {
            SystemState.State.Unknown -> GreenLocationPolicyManager.get(this)
            SystemState.State.GreenSilent -> GreenLocationPolicyManager.get(this)
            SystemState.State.GreenActive -> GreenLocationPolicyManager.get(this)
            SystemState.State.Yellow -> RegisteredUserLocationPolicyManager.get(this)
            SystemState.State.Red -> RegisteredUserLocationPolicyManager.get(this)
        }

    private val binder = WorkerBinder()

    inner class WorkerBinder : Binder() {
        fun getService() = this@WorkerService
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    companion object {
        private const val LOG_TAG = "WorkerService"
        private val yellowInitExecuted = AtomicBoolean(false)
        private val greenInitExecuted = AtomicBoolean(false)

        private val oneShotActions = setOf(CommonConsts.INIT_GREEN_ACTION, CommonConsts.INIT_YELLOW_ACTION)
        private val executedActions = mutableSetOf<String>()
    }
}
