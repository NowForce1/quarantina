package com.snap.apis.isolationguard.utils

import kotlinx.coroutines.CompletableDeferred
import java.util.concurrent.Executors
import java.util.concurrent.Future

object Threads {
    private val executor = Executors.newFixedThreadPool(10)
    fun <T> submit(f: () -> T?): Future<*> = executor.submit {
        try {
            f.invoke()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    suspend fun <T> withBlocking(f: () -> T) = withDeferred(f).await()

    fun <T> withDeferred(f: () -> T) = CompletableDeferred<T>().also { d ->
        submit {
            try {
                d.complete(f.invoke())
            } catch (x: Exception) {
                d.completeExceptionally(x)
            }
        }
    }
}