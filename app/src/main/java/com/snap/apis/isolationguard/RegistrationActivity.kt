package com.snap.apis.isolationguard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.snap.apis.isolationguard.forms.FormsFragment

class RegistrationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        supportFragmentManager.beginTransaction().add(R.id.registrationRoot, FormsFragment(), "Forms").commit()
    }
}
