package com.snap.apis.isolationguard.api

import android.content.Context
import android.content.Intent
import com.google.gson.Gson
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.CommonConsts
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Description:
 * Created by shmuel on 2019-05-30.
 */
object LoggedInConfig {
    private val contextRef = WeakReference<Context>(null)
    private val onValid = ConcurrentLinkedQueue<() -> Unit>()
    fun store(userDetails: UserDetails, transceiver: NFHttpClient, serverPrefix: String) {
        LoggedInConfig.userDetails = userDetails
        LoggedInConfig.transceiver = transceiver
        LoggedInConfig.serverPrefix = serverPrefix
        whenValid { }
    }

    fun save(context: Context) {
        Prefs.write(context, USER_DETAILS, Gson().toJson(userDetails))
        Prefs.write(context, SERVER_PREFIX, serverPrefix)
        Prefs.write(context, ACCESS_TOKEN, transceiver.token)
        Prefs.write(context, DEVICE_ID, deviceId)
    }

    fun load(context: Context): Boolean {
        serverPrefix = Prefs.read(context, SERVER_PREFIX, "")
        userDetails = Prefs.read(context, USER_DETAILS, "").takeIf { it.isNotEmpty() }?.let { Gson().fromJson(it, UserDetails::class.java) } ?: return false
        transceiver = Prefs.read(context, ACCESS_TOKEN, "").takeIf { it.isNotEmpty() }?.let { NFHttpClient(it) } ?: return false
        deviceId = Prefs.read(context, DEVICE_ID, "")
        return isValid()
    }

    fun isValid() = LoggedInConfig::userDetails.isInitialized && userDetails.userId > 0
    fun invalidate() {
        userDetails = UserDetails(null, -1L, -1)
        transceiver = NFHttpClient("")
        save(contextRef.get() ?: return)
        contextRef.get()?.sendBroadcast(Intent(CommonConsts.INVALID_CONFIG_ACTION))
    }

    @Suppress("ControlFlowWithEmptyBody")
    fun whenValid(function: () -> Unit) {
        onValid.add(function)
        if (isValid()) {
            while (onValid.poll()?.invoke() != null);
        }
    }

    lateinit var userDetails: UserDetails
    var troika: ConfigurationTroika? = null
    var locations: Map<LocationPolicyData.SettingType, List<LocationPolicyData.LocationPolicy.LocationPolicyEntity>>? = null
    var deviceId: String = ""

    private lateinit var serverPrefix: String
    lateinit var transceiver: NFHttpClient

    val urlPrefix get() = "https://$serverPrefix/api/${Locale.getDefault().run { "$language-$country" }}"

    private const val USER_DETAILS = "UserDetails"
    private const val SERVER_PREFIX = "ServerPrefix"
    private const val ACCESS_TOKEN = "AccessToken"
    private const val DEVICE_ID: String = "DeviceId"
}