package com.snap.apis.isolationguard.utils.extensions

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.view.children

val EditText.str get() = text.toString()
fun EditText.addTextChangedLambda(f: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = f(s.toString())
    })
}
fun ViewGroup.inflateChild(layout: Int): View = LayoutInflater.from(this.context).inflate(layout, this, false)
fun RadioGroup.checkedRadioButton() = children.find { (it as? RadioButton)?.isChecked == true }