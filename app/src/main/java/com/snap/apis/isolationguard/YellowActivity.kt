package com.snap.apis.isolationguard

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.snap.apis.isolationguard.api.LoggedInConfig
import com.snap.apis.isolationguard.location.LocationDriver
import com.snap.apis.isolationguard.location.RegisteredUserLocationPolicyManager
import com.snap.apis.isolationguard.utils.*
import com.snap.apis.isolationguard.utils.extensions.orIfNullOrEmpty
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CompletableDeferred

class YellowActivity : AppCompatActivity() {
    private val permissionsDeferred = CompletableDeferred<Boolean>()
    private val viewModel: MainActivityViewModel by lazy { ViewModelProvider(this, ViewModelFactory(this)).get(MainActivityViewModel::class.java) }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        ContextCompat.startForegroundService(
            this,
            Intent(this, WorkerService::class.java).setAction(CommonConsts.INIT_YELLOW_ACTION)
        )

        launchUI {
            if (hasLocationPermission()) {
                permissionsDeferred.complete(true)
                return@launchUI
            }

            requestPermissionsSuspend(getLocationRationale())

            if (!permissionsDeferred.await()) {
                return@launchUI
            }

            setupControls()
        }
    }

    private fun setupControls() {
        val activity = this@YellowActivity
        configLiveData.observe(activity, Observer { (dt, dx) ->
            timeConfig.progress = dt.sec.toInt()
            distanceConfig.progress = dx.toInt()

            currTimeCfg.text = timeFormatter(dt.ms)
            currDistanceCfg.text = distFormatter(dx)
        })

        val locationPM = viewModel.getLocationPM()
        val centrePoint = viewModel.getCentrePoint()
        centrePointText.text = centrePoint?.let { it.name.orIfNullOrEmpty("${it.lat},${it.lon}") } ?: getString(R.string.noCentrePoint)

        locationPM.missLiveData.observe(activity, Observer { (d, inRange) ->
            centrePointStatusText.text = if (inRange) getString(R.string.inRange) else getString(R.string.missedRangeBy, d.toInt())
            centrePointStatusText.setTextColor(if (inRange) Color.GREEN else Color.RED)
        })

        LoggedInConfig.whenValid {
            configLiveData.postValue(locationPM.config)
        }
        timeConfig.setOnSeekBarChangeListener(SeekBarListener(currTimeCfg, { activity.timeFormatter(it) }) {
            locationPM.setDt(it.ms)
            configLiveData.postValue(locationPM.config)
        })
        distanceConfig.setOnSeekBarChangeListener(SeekBarListener(currDistanceCfg, { activity.distFormatter(it) }) {
            locationPM.setDx(it)
            configLiveData.postValue(locationPM.config)
        })

        val thresholdLiveData = MutableLiveData<Number>()
        thresholdLiveData.observe(this@YellowActivity, Observer {
            it ?: return@Observer
            currThresholdCfg.text = distFormatter(it)
            thresholdConfig.progress = viewModel.getZoneRadius()
        })
        thresholdLiveData.postValue(viewModel.getZoneRadius())
        thresholdConfig.setOnSeekBarChangeListener(SeekBarListener(currThresholdCfg, { activity.distFormatter(it) }) {
            viewModel.setZoneRadius(it)
        })
        overrideLocationConfigButton.setOnClickListener { v ->
            v.visibility = View.GONE
            overrideLocationGroup.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            permissionsDeferred.complete(hasLocationPermission())
        }
    }

    private fun timeFormatter(x: Number) = x.ms.hms.let { (_, m, s) -> "$m:$s" }
    private fun distFormatter(x: Number) = "${x.toInt()}m"

    private val configLiveData = MutableLiveData<LocationDriver.LocationConfig>()

    private class SeekBarListener(private val tv: TextView, private val formatter: (Number) -> String, private val f: (Int) -> Unit) :
        SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            tv.text = formatter(progress)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            seekBar?.run { f(progress) }
        }
    }
}

private class ViewModelFactory(val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(Context::class.java).newInstance(context)
    }
}

private class MainActivityViewModel(val context: Context) : ViewModel() {
    fun getLocationPM(): RegisteredUserLocationPolicyManager =
        RegisteredUserLocationPolicyManager.get(context)

    fun getCentrePoint(): CentrePointDesc? =
        LoggedInConfig.userDetails.userSettings?.addresses?.find { it.isPrimary }
            ?.let { CentrePointDesc(it.lat, it.lon, it.addressTitle ?: "") }

    fun setZoneRadius(threshold: Int) {
        getLocationPM().applyBoundary(threshold.toFloat())
    }

    fun getZoneRadius(): Int {
        return getLocationPM().boundary.radiusMeters.toInt()
    }

    data class CentrePointDesc(val lat: Double, val lon: Double, val name: String)
}