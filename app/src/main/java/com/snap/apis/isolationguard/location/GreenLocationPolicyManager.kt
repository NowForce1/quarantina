package com.snap.apis.isolationguard.location

import android.content.Context
import android.location.Location
import com.snap.apis.isolationguard.regionaldata.PublicConfig
import com.snap.apis.isolationguard.state.SystemState
import com.snap.apis.isolationguard.utils.Dt
import com.snap.apis.isolationguard.utils.delay
import com.snap.apis.isolationguard.utils.launchUI
import com.snap.apis.isolationguard.utils.sec

class GreenLocationPolicyManager private constructor(context: Context) : LocationPolicyManager(context) {
    private val database by lazy { LocationStorageModel().getDatabase(context) }
    private val locationDriver = LocationDriver()
    override fun setup(context: Context) {
        fun setByState(state: SystemState.State) {
            when (state) {
                SystemState.State.GreenSilent -> stop()
                SystemState.State.GreenActive -> start()
                else -> {
                }
            }
        }
        SystemState.get(context).registerForState { (_, newState) -> setByState(newState) }
        setByState(SystemState.get(context).state)

        PublicConfig.observable.register { data ->
            launchUI {
                delay(1.sec)
                // Last config test. Config may change, don't apply immediately
                if (data == PublicConfig.get()) {
                    stop()
                    start()
                }
            }
        }
    }

    private fun start() {
        val config = PublicConfig.get() ?: return
        if (LocationDriver.isActive) return
        locationDriver.start(context, LocationDriver.LocationConfig(config.locationSamplePeriod / 3, config.distanceMeters.toFloat().coerceIn(5f, 10f)))
    }

    private fun stop() {
        if (!LocationDriver.isActive) return
        LocationDriver.stop(context)
    }

    override suspend fun onLocation(location: Location) {
        if (SystemState.get(context).state == SystemState.State.GreenSilent) return
        if (!credibilityTest(location)) return
        database.locationsDao().insert(location.toEntry(true, SystemState.State.GreenActive.colour))
    }

    override fun isAccepted(location: Location, dtFromThatSource: Dt): Boolean = PublicConfig.get()?.let { dtFromThatSource > (it.locationSamplePeriod / 5) } == true

    fun startOrStop(start: Boolean) {
        if (start) {
            start()
        } else {
            LocationDriver.stop(context)
        }
    }


    companion object {
        private var instance: GreenLocationPolicyManager? = null
        fun get(context: Context) =
            instance ?: GreenLocationPolicyManager(context.applicationContext).also { instance = it }
    }
}