package com.snap.apis.isolationguard.location

import android.content.Context
import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.snap.apis.isolationguard.api.LocationPolicyData
import com.snap.apis.isolationguard.api.LocationTransmitter
import com.snap.apis.isolationguard.api.LoggedInConfig
import com.snap.apis.isolationguard.state.SystemState
import com.snap.apis.isolationguard.storage.Prefs
import com.snap.apis.isolationguard.utils.*
import kotlin.math.abs
import kotlin.properties.Delegates

class RegisteredUserLocationPolicyManager private constructor(context: Context) : LocationPolicyManager(context) {
    private val database by lazy { LocationStorageModel().getDatabase(context) }
    private val driver = LocationDriver()
    private val observable = Observable<Location>()
    var config by Delegates.observable(
        LocationDriver.LocationConfig(1.min, 500f)
    ) { _, oldConfig, newConfig ->
        if (oldConfig != newConfig) {
            Log.v(LOG_TAG, "Location Config = $oldConfig -> $newConfig")
            LocationDriver.stop(context)
            driver.start(context, newConfig)
        }
    }
        private set

    var boundary: Boundary by Delegates.observable(Boundary(null, getThreshold(context))) { _, old, new ->
        Log.v(LOG_TAG, "Boundary: $old -> $new")
    }
        private set

    data class Boundary(val centrePoint: Location?, val radiusMeters: Float)

    private var lastSuccessfulSend = -1L

    init {
        applyConfig()
    }

    fun setDt(dt: Dt) {
        config = LocationDriver.LocationConfig(dt, config.dx)
    }

    fun setDx(dx: Number) {
        config = LocationDriver.LocationConfig(config.dt, dx.toFloat())
    }

    fun applyConfig() {
        config =
            LoggedInConfig.locations?.get(LocationPolicyData.SettingType.EN_ROUTE)?.firstOrNull()
                ?.run { LocationDriver.LocationConfig(rateSec.sec, rateMeters) }
                ?: LocationDriver.LocationConfig(1.min, 500f)
    }

    fun applyBoundary(threshold: Float? = null) {
        if (threshold != null) {
            Prefs.write(context, "DxThreshold", threshold)
        }
        val historyThreshold = threshold ?: getThreshold(context)
        boundary = Boundary(calculateCentrePoint(), historyThreshold)
    }

    private fun getThreshold(context: Context) = Prefs.read(context, "DxThreshold", 500f)

    private fun calculateCentrePoint(): Location? {
        return LoggedInConfig.userDetails.userSettings?.addresses?.find { it.isPrimary }
            ?.let { Location("Primary").apply { latitude = it.lat; longitude = it.lon } }
    }

    data class BoundaryObedience(val distance: Float, val inBoundary: Boolean)

    val missLiveData = MutableLiveData<BoundaryObedience>()

    override fun isAccepted(location: Location, dtFromThatSource: Dt) =
        dtFromThatSource > config.dt / 2

    override fun setup(context: Context) {
        val systemState = SystemState.get(context)
        systemState.registerForState { (_, new) ->
            when (new) {
                SystemState.State.Unknown -> {
                }
                SystemState.State.GreenSilent -> {
                }
                SystemState.State.GreenActive -> {
                }
                SystemState.State.Yellow -> {
                    driver.start(context, config)
                }
                SystemState.State.Red -> {
                    driver.start(context, config)
                }
            }
        }
    }

    override suspend fun onLocation(location: Location) {
        DeviceStatus.sampleBatteryLevel(context)
        if (!credibilityTest(location)) return
        onLocationInYellowState(location)
    }

    private suspend fun onLocationInYellowState(location: Location) {
        val sendLocations = if (lastSuccessfulSend < 0) {
            lastSuccessfulSend = database.lastLocationSentDao().getLastSuccessfulSendTime() ?: -1L
            true
        } else {
            val centrePoint = boundary.centrePoint
            val distanceFromPrimary = if (centrePoint != null) abs(
                (location.distanceTo(centrePoint) - location.accuracy).coerceAtLeast(0f)
            ) else 0f
            val outOfRange = distanceFromPrimary > boundary.radiusMeters
            missLiveData.postValue(BoundaryObedience(distanceFromPrimary, !outOfRange))
            if (outOfRange) {
                Log.v(
                    LOG_TAG,
                    "User is $distanceFromPrimary away from centre. Th=${boundary.radiusMeters}"
                )
                database.locationsDao()
                    .insert(location.toEntry(true, SystemState.State.Yellow.colour))
                observable.call(location)
                true
            } else false
        }

        if (sendLocations) {
            sendLocationsToServer()
        }
    }

    private suspend fun sendLocationsToServer() {
        if (lastSuccessfulSend < 0) {
            lastSuccessfulSend = database.lastLocationSentDao().getLastSuccessfulSendTime() ?: -1L
        }

        val locationsSince = database.locationsDao().getLocationsSince(lastSuccessfulSend)
            .map(this::locationFromEntry)
        val now = TimeUtils.now
        val success = when (locationsSince.size) {
            0 -> return
            1 -> {
                val location = locationsSince.first()
                val retcode = Threads.withBlocking { LocationTransmitter.send(location) }
                database.lastLocationSentDao()
                    .insert(LocationSentEntry(0, now, location.time, 1, retcode.isSuccess))
                retcode.isSuccess
            }
            else -> {
                val retcode = Threads.withBlocking { LocationTransmitter.send(locationsSince) }
                database.lastLocationSentDao().insert(
                    LocationSentEntry(
                        0,
                        now,
                        locationsSince.first().time,
                        locationsSince.size,
                        retcode.isSuccess
                    )
                )
                retcode.isSuccess
            }
        }

        if (success) {
            lastSuccessfulSend = now
        }
    }

    companion object {
        private var instance: RegisteredUserLocationPolicyManager? = null

        @Synchronized
        fun get(context: Context): RegisteredUserLocationPolicyManager {
            return instance ?: RegisteredUserLocationPolicyManager(context).also {
                instance = it
                Log.v(LOG_TAG, "Creating PM for the 1st time")
                it.driver.start(context.applicationContext, it.config)
            }
        }

        fun stop(context: Context) {
            LocationDriver.stop(context.applicationContext)
        }

        private const val LOG_TAG = "LocationPolicyManager"
    }
}
