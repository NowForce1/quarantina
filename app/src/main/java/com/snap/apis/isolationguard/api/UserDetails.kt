package com.snap.apis.isolationguard.api

import com.google.gson.annotations.SerializedName

data class UserDetailsResponse(val MobileUser: UserDetails)

data class UserDetails(
    @SerializedName("UserName") val username: String?,
    @SerializedName("UserId") val userId: Long,
    @SerializedName("OrganizationId") val orgId: Int,

    @SerializedName("SecurityToken") val securityToken: String = "",

    @Suppress("SpellCheckingInspection")
    @SerializedName("LocationLongtitude") val lon: Double = -1000.0,
    @SerializedName("LocationLatitude") val lat: Double = -1000.0,
    @SerializedName("Floor") val floor: String? = null,
    @SerializedName("MobileUserSettings") val userSettings: MobileUserSettings? = null
) {
    data class MobileUserSettings(@SerializedName("MobileUsersAdresses") val addresses: List<UserAddress>) {
        data class UserAddress(
            @SerializedName("AddressType") val type: Int,
            @SerializedName("IsPrimary") val isPrimary: Boolean,
            @SerializedName("Latitude") val lat: Double,
            @SerializedName("Longitude") val lon: Double,
            @SerializedName("AddressTitle") val addressTitle: String? = ""
        )
    }
}