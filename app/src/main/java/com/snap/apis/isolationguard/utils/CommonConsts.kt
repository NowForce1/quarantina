package com.snap.apis.isolationguard.utils

object CommonConsts {
    const val FIRST_INIT_EXTRA = "FirstInit"

    const val INVALID_CONFIG_ACTION = "com.snap.apis.isolationguard.INVALID_CONFIG"
    const val ON_LOCATION_ACTION = "com.snap.apis.isolationguard.ON_LOCATION"
    const val INIT_YELLOW_ACTION = "com.snap.apis.isolationguard.INIT_YELLOW_ACTION"
    const val INIT_GREEN_ACTION = "com.snap.apis.isolationguard.INIT_GREEN_ACTION"
    const val REFRESH_ALMANAC_ACTION = "com.snap.apis.isolationguard.REFRESH_ALMANAC_ACTION"
    const val SCHEDULE_REFRESH_ALMANAC_ACTION = "com.snap.apis.isolationguard.SCHEDULE_REFRESH_ALMANAC_ACTION"
    const val SCHEDULE_REFRESH_ALMANAC_TIME_EXTRA = "com.snap.apis.isolationguard.SCHEDULE_REFRESH_ALMANAC_TIME_EXTRA"
    object Uris {

        const val PUBLIC_CONFIG_URI = "https://nf-corona.s3.amazonaws.com/19/config.json"
        const val ALMANAC_DEFAULT_URI = "https://nf-corona.s3.amazonaws.com/19/locations.json"
    }

    object Debug {
        const val FAKE_CONFIG = "FakeConfig"
    }
}