package com.snap.apis.isolationguard.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.snap.apis.isolationguard.R
import kotlin.coroutines.suspendCoroutine

fun Context.hasLocationPermission(): Boolean =
    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

fun Context.getLocationRationale() = getString(R.string.locationRationaleFormattedMessage, getString(R.string.app_name))

fun Activity.requestPermissionsAsync(rationale: String = getLocationRationale()) {
    launchUI { requestPermissionsSuspend(rationale) }
}

suspend fun Activity.requestPermissionsSuspend(locationRationale: String = getLocationRationale()) {
    if (!hasLocationPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val permissions = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
            else -> emptyArray()
        }

        if (permissions.any { ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }) {
            val dialogueResult = suspendCoroutine<Boolean> { c ->
                AlertDialog.Builder(this).setTitle(R.string.warning).setMessage(locationRationale).setIcon(android.R.drawable.stat_sys_warning)
                    .setNegativeButton(R.string.deny) { dialog: DialogInterface?, _ -> dialog?.dismiss(); c.safeResume(false) }
                    .setPositiveButton(R.string.acceptPermission) { dialog: DialogInterface?, _ -> dialog?.dismiss(); c.safeResume(true) }
                    .setOnDismissListener { c.safeResume(false) }.show()
            }

            if (!dialogueResult) {
                return
            }
        }

        requestPermissions(permissions, 1)
    }

}
